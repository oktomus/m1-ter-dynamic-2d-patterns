**Sujet**: Synthèse procédurale de motifs sur une surface dans l’espace image

**Encadrement** : Pascal Guehl (p.guehl@unistra.fr), Rémi Allègre (remi.allegre@unistra.fr)

**Etudiant** : Kevin Masson (k.masson@unistra.fr)

-----------

| Branches                                                                                  | Description                                         |    
| -------------                                                                             |-------------                                        | 
| [master](https://git.unistra.fr/kmasson/TER_Dynamic2DPatterns/tree/master)                | Branche principale, contient le code source         |
| [doc](https://git.unistra.fr/kmasson/TER_Dynamic2DPatterns/tree/doc)                      | Documents liés au projet, rapport, feuille de route |
| [presentation](https://git.unistra.fr/kmasson/TER_Dynamic2DPatterns/tree/presentation)    | Présentations réalisées                             |

