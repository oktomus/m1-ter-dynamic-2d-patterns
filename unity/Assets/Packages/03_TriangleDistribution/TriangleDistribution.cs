﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Test different sampling techniques on triangles
 */

[RequireComponent(typeof(MeshFilter))]
public class TriangleDistribution : MonoBehaviour {

	private struct Interval
	{
		public float min;
		public float max;
	};

	/**
	 * Method used for distribution
	 * 1: Naive
	 */
	[SerializeField] protected int method = 1;
	[SerializeField] protected int amountPerTriangle = 1;
	public GameObject voxel;
	public float lambda = 5;
	public float phy = 1;

	/**
	 * Redistribute the points
	 */
	[SerializeField] protected bool refresh = true;

	[SerializeField] protected float sampleSize = 0.02f;

	private float voxel_edge_length;
	private float voxel_edge_threshold;
	private float voxel_radius;
	private Vector3 voxel_center;
	private Vector3 voxel_b1;
	private Vector3 voxel_b2;

	

	/**
	 * Samples for each triangle, ordered as the mesh triangle list
	 */
	private List<Vector3> samples = new List<Vector3>();
	private List<Color> samplesColor = new List<Color> ();

    // Use this for initialization
    void Start() {
		int queue_max_size = 3;
		int queue_size = 0;
		int [] queue = new int[queue_max_size];

		int [] to_add = new int[] {10, 40, 20, 30, 90, 100, 20};

		for (int x = 0; x < to_add.Length; ++x)
        {
            int new_element = to_add[x];
			if (new_element == 90)
				queue_size = 1;
            Debug.Log("Addind " + new_element);

            bool room_left = queue_max_size > queue_size;
            bool swap_right = false;
            int last_element = 0;
            int tmp_element = 0;
            // Find the index where to ADD
            for (int i = 0; i < queue_max_size; ++i)
            {
                // We are in an empty space
                // Meaning this is the first element or the higher one
                if (i > (queue_size - 1))
                {
                    queue[i] = new_element;
                    queue_size++;
                    break;
                }

				if (room_left)
				// In the case where the queue is not full
				{
                    if (queue[i] > new_element && !swap_right)
                    {
                        swap_right = true;
                        last_element = queue[i];
                        queue[i] = new_element;
                        queue_size++;
						continue;
                    }

                    if (swap_right)
                    {
                        tmp_element = queue[i];
                        queue[i] = last_element;
                        last_element = tmp_element;
						if (i >= (queue_size - 1)) break;
                    }
                }
				else
				// In the case where the queue is full
				{
					if (queue[i] > new_element) break;
					if (i == queue_max_size - 1) // Last element
					{
						queue[i] = new_element;
						break;
					}
					if (queue[i + 1] >= new_element)
					{
						queue[i] = new_element;
						break;
					}
                    queue[i] = queue[i + 1];
				}

            }

			Debug.Log("QUEUE = " +string.Join(", ",
             new List<int>(queue)
             .ConvertAll(i => i.ToString())
             .ToArray()));
        }

    }
	
	// Update is called once per frame
	void Update () {
		// Compute voxels values

		Mesh voxel_mesh = voxel.GetComponent<MeshFilter>().mesh;
		voxel_center = voxel.transform.position;

		Vector3 ea = voxel.transform.TransformPoint(voxel_mesh.vertices[0]);
		Vector3 eb = voxel.transform.TransformPoint(voxel_mesh.vertices[1]);
		voxel_edge_length = Vector3.Distance(ea, eb);
		voxel_edge_threshold = voxel_edge_length * phy;
		voxel_radius = voxel_edge_length / 2.0f;
		voxel_b1 = voxel_center;
		voxel_b1.x -= voxel_radius;
		voxel_b1.y -= voxel_radius;
		voxel_b1.z -= voxel_radius;
		voxel_b2 = voxel_center;
		voxel_b2.x += voxel_radius;
		voxel_b2.y += voxel_radius;
		voxel_b2.z += voxel_radius;

		if (refresh || amountPerTriangle * samplesColor.Count != samples.Count) {

			samples.Clear ();
			samplesColor.Clear ();

			var mesh = GetComponent<MeshFilter> ().mesh;
			var verts = mesh.vertices;
			var triangles = mesh.triangles;

			for (int i = 0; i < triangles.Length; i+=3) {
				Vector3 a = verts [triangles [i]];
				Vector3 b = verts [triangles [i + 1]];
				Vector3 c = verts [triangles [i + 2]];
				Vector3 sample = new Vector3();

				for (int j = 0; j < amountPerTriangle; j++) {
                    bool add = false;
					switch (method) {
					case 1:
						sample = UniformSampling (a, b, c);
						add = true;
						break;
					case 2:
						add = ExponentialSampling(a, b, c, out sample);
						break;
					case 3:
						add = FlatExponentialSampling(a, b, c, out sample);
						break;
					default:
						break;

					}
                    if (add) samples.Add(sample);
				}
				samplesColor.Add (Random.ColorHSV ());
			}							


			refresh = false;
		}
		
	}

	/**
	 * Naive sampling
	 */
	public static Vector3 UniformSampling(Vector3 a, Vector3 b, Vector3 c) {
		float ua = Random.Range (0.0f, 1.0f);
		float ub = Random.Range (0.0f, 1.0f);
		if (ua + ub > 1.0f) {
			ua = 1 - ua;
			ub = 1 - ub;
		}
		Vector3 res = a + ua * (b - a) + ub * (c - a);
		return res;
	}

	private struct ExponentialSamplingTriangleElement
	{
		public Vector3 a;
		public Vector3 b;
		public Vector3 c;
		public float priority;
	};

	static private int AddElementInPriorityList(
		ExponentialSamplingTriangleElement[] queue,
		int queue_size,
		ExponentialSamplingTriangleElement new_element
	)
	{
		bool room_left = queue.Length > queue_size;
		bool swap_right = false;
		ExponentialSamplingTriangleElement last_element = new ExponentialSamplingTriangleElement();
		ExponentialSamplingTriangleElement tmp_element = new ExponentialSamplingTriangleElement();

		for (int i = 0; i < queue.Length; ++i)
		{
			// If it's empty
			if (i > (queue_size - 1))
			{
				queue[i] = new_element;
				queue_size++;
				break;
			}

			// If the queue has some room left
			if (room_left)
			{
				// This is where we add the new element
				if (queue[i].priority > new_element.priority && !swap_right)
				{
					swap_right = true;
					last_element = queue[i];
					queue[i] = new_element;
					queue_size++;
					continue;
				}

				// We already added the element, shifting required
				if (swap_right)
				{
					tmp_element = queue[i];
					queue[i] = last_element;
					last_element = tmp_element;
					if (i >= (queue_size - 1)) break; // Nothing after 
				}
			}
			// If the queue is full
			else
			{
				if (queue[i].priority > new_element.priority) break;

				if (i == queue.Length - 1)
				{
					queue[i] = new_element;
					break;
				}

				if (queue[i + 1].priority >= new_element.priority)
				{
					queue[i] = new_element;
					break;
				}

				queue[i] = queue[i + 1];
			}
		}

		return queue_size;
	}

	static private float TriangleArea(Vector3 a, Vector3 b, Vector3 c)
	{
		float dista = Vector3.Distance(a, b);
		float distb = Vector3.Distance(b, c);
		float distc = Vector3.Distance(c, a);
		float s = (dista + distb + distc) * 0.5f;

		return Mathf.Sqrt(s * (s - dista) * (s - distb) * (s - distc));
	}

	public bool FlatExponentialSampling(Vector3 a, Vector3 b, Vector3 c, out Vector3 sample) {

		sample = new Vector3();

        // Triangle inside the box ?
        if (!TriangleAABB(voxel_b1, voxel_b2, a, b, c)) return false;

		// Priority list
		// We add the element with the top priority at the end
		int queue_max_size = 10;
		ExponentialSamplingTriangleElement [] queue = new ExponentialSamplingTriangleElement[queue_max_size];

		// Init
		int queue_size = 1;
		queue[0].priority = 0;
		queue[0].a = a;
		queue[0].b = b;
		queue[0].c = c;

		// Process
		while (queue_size > 0)
		{
			// Take the last element
			queue_size--;
			a = queue[queue_size].a;
			b = queue[queue_size].b;
			c = queue[queue_size].c;

            // Triangle too big ?
            float distab = Vector3.Distance(a, b),
                distbc = Vector3.Distance(b, c),
                distca = Vector3.Distance(c, a);

			// If not too big, try uniform sampling
			if (distab <= voxel_edge_threshold && distbc <= voxel_edge_threshold && distca <= voxel_edge_threshold)
            {
                // Do uniform sampling
                sample = UniformSampling(a, b, c);
                return true;
            }

			// We will create 2 new triangle
			Vector3 new_t1_a = new Vector3();
			Vector3 new_t1_b = new Vector3();
			Vector3 new_t1_c = new Vector3();
			Vector3 new_t2_a = new Vector3();
			Vector3 new_t2_b = new Vector3();
			Vector3 new_t2_c = new Vector3();

            // Find an edge to subdivide
            if (distab >= distbc && distab >= distca)
            {
                // Split edge AB
                Vector3 new_point = (a + b) / 2.0f;
				// New triangles points
				new_t1_a = a;
				new_t1_b = new_t2_b = new_point;
				new_t1_c = new_t2_a = c;
				new_t2_c = b;
            }
            else if (distbc >= distab && distbc >= distca)
            {
                // Split edge BC
                Vector3 new_point = (b + c) / 2.0f;
				// New triangles points
				new_t1_a = b;
				new_t1_b = new_t2_b = new_point;
				new_t1_c = new_t2_a = a;
				new_t2_c = c;
            }
            else if (distca >= distab && distca >= distbc)
            {
                // Split edge CA
                Vector3 new_point = (c + a) / 2.0f;
				// New triangles points
				new_t1_a = c;
				new_t1_b = new_t2_b = new_point;
				new_t1_c = new_t2_a = b;
				new_t2_c = a;
            }

			// Add each new triangle in the queue if it's in the voxel
			if (TriangleAABB(voxel_b1, voxel_b2, new_t1_a, new_t1_b, new_t1_c))
			{
				ExponentialSamplingTriangleElement new_element = new ExponentialSamplingTriangleElement(); 
				new_element.a = new_t1_a; 
				new_element.b = new_t1_b; 
				new_element.c = new_t1_c;
				new_element.priority =  
					TriangleArea(new_t1_a, new_t1_b, new_t1_c) * 
					lambda * Mathf.Exp(-lambda * Vector3.Distance((new_t1_a + new_t1_b + new_t1_c) / 3.0f, voxel_center));

				queue_size = AddElementInPriorityList(queue, queue_size, new_element);
			}

			if (TriangleAABB(voxel_b1, voxel_b2, new_t2_a, new_t2_b, new_t2_c))
			{
				ExponentialSamplingTriangleElement new_element = new ExponentialSamplingTriangleElement(); 
				new_element.a = new_t2_a; 
				new_element.b = new_t2_b; 
				new_element.c = new_t2_c;
				new_element.priority =  
					TriangleArea(new_t2_a, new_t2_b, new_t2_c) *
					lambda * Mathf.Exp(-lambda * Vector3.Distance((new_t2_a + new_t2_b + new_t2_c) / 3.0f, voxel_center));

				queue_size = AddElementInPriorityList(queue, queue_size, new_element);
			}
		}
		return false;
	}

	public bool ExponentialSampling(Vector3 a, Vector3 b, Vector3 c, out Vector3 sample) {
		sample = new Vector3();
		if (!TriangleAABB(voxel_b1, voxel_b2, a, b, c)) return false;

		float distab = Vector3.Distance(a, b),
			distbc = Vector3.Distance(b, c),
			distca = Vector3.Distance(c, a);
		
		// Find an edge to subdivide
		if (distab > voxel_edge_threshold && distab > distbc && distab > distca)
		{
			// Split edge AB
			Vector3 new_point = (a + b) / 2.0f;
			// Prioritize
			float pb_t1 = lambda * Mathf.Exp(- lambda * Vector3.Distance((a + new_point + c) / 3.0f, voxel_center));
			float pb_t2 = lambda * Mathf.Exp(- lambda * Vector3.Distance((c + new_point + b) / 3.0f, voxel_center));
			if (pb_t1 > pb_t2)
			{
                if (ExponentialSampling(a, new_point, c, out sample)) return true;
                if (ExponentialSampling(c, new_point, b, out sample)) return true;
            }
			else
			{
                if (ExponentialSampling(c, new_point, b, out sample)) return true;
                if (ExponentialSampling(a, new_point, c, out sample)) return true;
			}
			return false;
        }
		else if (distbc > voxel_edge_threshold && distbc > distab && distbc > distca)
		{
			// Split edge BC
			Vector3 new_point = (b + c) / 2.0f;
			// Prioritize
			float pb_t1 = lambda * Mathf.Exp(- lambda * Vector3.Distance((b + new_point + a) / 3.0f, voxel_center));
			float pb_t2 = lambda * Mathf.Exp(- lambda * Vector3.Distance((a + new_point + c) / 3.0f, voxel_center));
			if (pb_t1 > pb_t2)
			{
                if (ExponentialSampling(b, new_point, a, out sample)) return true;
                if (ExponentialSampling(a, new_point, c, out sample)) return true;
			}
			else
			{
                if (ExponentialSampling(a, new_point, c, out sample)) return true;
                if (ExponentialSampling(b, new_point, a, out sample)) return true;
			}
			return false;
		}
		else if (distca > voxel_edge_threshold && distca > distab && distca > distbc)
		{
			// Split edge CA
			Vector3 new_point = (c + a) / 2.0f;
			// Prioritize
			float pb_t1 = lambda * Mathf.Exp(- lambda * Vector3.Distance((c + new_point + b) / 3.0f, voxel_center));
			float pb_t2 = lambda * Mathf.Exp(- lambda * Vector3.Distance((b + new_point + a) / 3.0f, voxel_center));
			if (pb_t1 > pb_t2)
			{
                if (ExponentialSampling(c, new_point, b, out sample)) return true;
                if (ExponentialSampling(b, new_point, a, out sample)) return true;
			}
			else
			{
                if (ExponentialSampling(b, new_point, a, out sample)) return true;
                if (ExponentialSampling(c, new_point, b, out sample)) return true;
			}
			return false;
		}
		else
		{
			// Do uniform sampling
			sample = UniformSampling(a, b, c);
			return true;
		}
	}

	private static Interval GetInterval(Vector3 ta, Vector3 tb, Vector3 tc, Vector3 axis)
	{
		Interval result;
		// Project points on the axis
		float p1 = Vector3.Dot(ta, axis);
		float p2 = Vector3.Dot(tb, axis);
		float p3 = Vector3.Dot(tc, axis);

		result.max = Mathf.Max(p1, Mathf.Max(p2, p3));
		result.min = Mathf.Min(p1, Mathf.Min(p2, p3));

		return result;
	}

	private static Interval GetInterval(Vector3 b1, Vector3 b2, Vector3 axis)
	{
		Interval result;

		Vector3 [] vertex = new Vector3[]{
			new Vector3(b1.x, b2.y, b2.z),
			new Vector3(b1.x, b2.y, b1.z),
			new Vector3(b1.x, b1.y, b1.z),
			new Vector3(b1.x, b1.y, b2.z),
			new Vector3(b2.x, b2.y, b2.z),
			new Vector3(b2.x, b2.y, b1.z),
			new Vector3(b2.x, b1.y, b1.z),
			new Vector3(b2.x, b1.y, b2.z)
		};

		result.min = result.max = Vector3.Dot(axis, vertex[0]);

		for (int i = 1; i < 8; ++i)
		{
			float proj = Vector3.Dot(axis, vertex[i]);
			result.min = Mathf.Min(result.min, proj);
			result.max = Mathf.Max(result.max, proj);
		}

		return result;
	}

	private static bool OverlapOnAxis(Vector3 b1, Vector3 b2, Vector3 ta, Vector3 tb, Vector3 tc, Vector3 axis)
	{
		Interval a = GetInterval(b1, b2, axis);
		Interval b = GetInterval(ta, tb, tc, axis);
		return ((b.min <= a.max) && (a.min <= b.max));
	}

	private static bool TriangleAABB(Vector3 b1, Vector3 b2, Vector3 ta, Vector3 tb, Vector3 tc)
	{
		// Triangle edges
		Vector3 ab = tb - ta;
		Vector3 bc = tc - tb;
		Vector3 ca = ta - tc;

		// AABB Normals
		Vector3 u0 = new Vector3(1, 0, 0);
		Vector3 u1 = new Vector3(0, 1, 0);
		Vector3 u2 = new Vector3(0, 0, 1);

		Vector3 [] test_axis = new Vector3[]{
			u0, u1, u2, // AABB Normals
			Vector3.Cross(ab, bc), // Triangle normal
			Vector3.Cross(u0, ab), Vector3.Cross(u0, bc), Vector3.Cross(u0, ca),
			Vector3.Cross(u1, ab), Vector3.Cross(u1, bc), Vector3.Cross(u1, ca),
			Vector3.Cross(u2, ab), Vector3.Cross(u2, bc), Vector3.Cross(u2, ca)
			// Triangle / AABB cross products
		};

		for (int i = 0; i < 13; ++i)
		{
			if (!OverlapOnAxis(b1, b2, ta, tb, tc, test_axis[i]))
				return false;
		}

		return true;
	}



	private void OnDrawGizmos () {		
		/* 
		for(int i = 0; i < samplesColor.Count; i++)
		{
			Gizmos.color = samplesColor [i];
			for (int j = i * amountPerTriangle; j < i * amountPerTriangle + amountPerTriangle; j++) {
				Gizmos.DrawSphere (samples [j], sampleSize);
			}
		}					
		*/
		Gizmos.color = Color.red;
		for (int i = 0; i < samples.Count; ++i)
		{
			Gizmos.DrawSphere(samples[i], sampleSize);
		}
	}
}
