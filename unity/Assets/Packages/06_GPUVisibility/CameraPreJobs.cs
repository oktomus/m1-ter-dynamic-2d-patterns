﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPreJobs : MonoBehaviour {

	List<IPreFrameJob> callbacks = new List<IPreFrameJob>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddPreJob(IPreFrameJob obj) 
	{
		callbacks.Add(obj);
	}

	void OnPreRender()
	{
		for(int i = 0, n = callbacks.Count; i < n; i++) {
			IPreFrameJob f = callbacks[i];
			f.PreFrameJob();
		}
	}
}
