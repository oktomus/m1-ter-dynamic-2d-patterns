﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine.Rendering;

public interface IPreFrameJob
{
    void PreFrameJob();
}

public class GPUVisibilityTest : MonoBehaviour, IPreFrameJob {

	public Material weighterMaterial;
    public Material renderTextureDebugger;
    public ComputeShader computeShader;
	private ComputeBuffer samplesBuffer;

    public bool render;
    public bool continuous;

	private Mesh mesh;

    public int depth = 8;

    public float samplesize = 0.1f;

    public bool showPoints = false;

    private List<Vector3> trianglesPos;


    private RenderTexture rtex;

    private GPUSampling.Kernel computeLink;

    private int width, height;

    private List<bool> trianglesVisibility;
    private RenderTextureFormat format;

    private struct ComputeBufferStruct {
        //public int x;
        //public int y;
        //public bool flag;
        //public int width;
        //public int height;
        public int id; // = PrimitiveID + 1. 0 = Pas de triangle
    }

    private class DispComputeBuffer : System.IDisposable
    {
        public ComputeBuffer buffer;
        public DispComputeBuffer(ComputeBuffer buffer)
        {
            this.buffer = buffer;
        }

        public ComputeBufferStruct[] GetData()
        {
            var data = new ComputeBufferStruct[buffer.count];
            buffer.GetData(data);
            return data;
        }

        public void Dispose()
        {
            this.buffer.Release();
        }
    }

    private DispComputeBuffer computeBuffer;

    private ComputeBufferStruct[] computeData;
	
	// Use this for initialization
	void Start () {
        if(!SystemInfo.supportsComputeShaders)
        {
            Debug.LogError("Compute shaders are not supported");
            return;
        }
        Display mainDisp = Display.main;
        width = mainDisp.renderingWidth;
        height = mainDisp.renderingHeight;

        format = RenderTextureFormat.RInt;

        if (!SystemInfo.SupportsRenderTextureFormat(
            format
        ))
        {
            Debug.LogError("Render format is not supported");
            return;
        }

        // Attach job
        Camera.main.GetComponent<CameraPreJobs>().AddPreJob(this);

        mesh = GetComponent<MeshFilter>().mesh;
        trianglesVisibility = new List<bool>();
        trianglesPos = new List<Vector3>();

        // Fill triangle positions
        for(int i = 0; i < mesh.triangles.Length; i+=3)
        {
            int ia = mesh.triangles[i];
            int ib = mesh.triangles[i + 1];
            int ic = mesh.triangles[i + 2];

            Vector3 a = mesh.vertices[ia];
            Vector3 b = mesh.vertices[ib];
            Vector3 c = mesh.vertices[ic];

            trianglesPos.Add((a + b + c) / 3.0f);
            trianglesVisibility.Add(false);
        }

        // Create compute buffer
        Debug.Log(trianglesVisibility.Count + " triangles");
    }

    public void PreFrameJob()
    {
        if(!render || mesh == null) return;
        if(!continuous) render = false;
        Debug.Log("Starting pre frame job");
        //width = height = 10;

        // Render
        float then = Time.realtimeSinceStartup;
        if(rtex == null)
        {
            // Create render texture
            rtex = new RenderTexture(
                width, height, depth, format, RenderTextureReadWrite.Linear
            );
        }
        // Clear previous render
        RenderTexture rt = UnityEngine.RenderTexture.active;
        UnityEngine.RenderTexture.active = rtex;
        GL.Clear(true, true, Color.clear);
        UnityEngine.RenderTexture.active = rt;
        // Debug texture on plain
        if(renderTextureDebugger != null)
        {
            renderTextureDebugger.mainTexture = rtex;
        }
        // Real job 
        Graphics.SetRenderTarget(rtex);
        if(!weighterMaterial.SetPass(0))
        {
            Debug.LogError("Failed to sed rendering path");
            return;
        }
        // Set correct matrix
        Matrix4x4 P = GL.GetGPUProjectionMatrix(Camera.main.projectionMatrix, true);
        Matrix4x4 V = Camera.main.worldToCameraMatrix;
        Matrix4x4 M = transform.localToWorldMatrix;
        weighterMaterial.SetMatrix("_MATRIX_M", M);
        weighterMaterial.SetMatrix("_MATRIX_V", V);
        weighterMaterial.SetMatrix("_MATRIX_P", P);
        weighterMaterial.SetFloat("_triangleCount", mesh.triangles.Length / 3.0f);
        // DRAW !
        Graphics.DrawMeshNow(mesh, transform.position, transform.rotation);

        float diff = Time.realtimeSinceStartup - then;
        Debug.Log("Rendering took " + diff + " seconds");

        //width= height = 8;
        // Compute pixels on another shader
        then = Time.realtimeSinceStartup;
        ComputeBuffer outBuffer = new ComputeBuffer(
            width * height,
            //mesh.triangles.Length / 3,
            Marshal.SizeOf(typeof(ComputeBufferStruct))
        );
        /* 
        REQUIRED TO RESET DATA
        */
        ComputeBufferStruct[] setData = new ComputeBufferStruct[outBuffer.count];
        ComputeBufferStruct val = new ComputeBufferStruct();
        val.id = 0;
        for(int i = 0; i < setData.Length; i++)
        {
            setData[i] = val;
        }
        outBuffer.SetData(setData);
        //*/
        /* 
        if(!computeShader.HasKernel("ComputeVisbility"))
        {
            Debug.LogError("Kernel not found");
            return;
        }
        computeLink = new GPUSampling.Kernel(computeShader, "ComputeVisbility");
        computeShader.SetInt("_Width", width);
        computeShader.SetInt("_Height", height);
        computeShader.SetInt("_trisCount", mesh.triangles.Length / 3);
        rtex.name = "_Texture";
        computeShader.SetTexture(computeLink.Index, "_Texture", rtex);
        computeShader.SetBuffer(computeLink.Index, "_VisbilityBuffer",
            outBuffer); 
            */
        // PREPARE
        if(!computeShader.HasKernel("ReadTextureIDS"))
        {
            Debug.LogError("Kernel not found");
            return;
        }
        computeLink = new GPUSampling.Kernel(computeShader, "ReadTextureIDS");
        computeShader.SetInt("_Width", width);
        computeShader.SetInt("_Height", height);
        rtex.name = "_Texture";
        computeShader.SetTexture(computeLink.Index, "_Texture", rtex);
        computeShader.SetBuffer(computeLink.Index, "_IDS",
            outBuffer); 

        // GO!
        computeShader.Dispatch(computeLink.Index,
            width / (int)computeLink.ThreadX + 1,
            height / (int)computeLink.ThreadY + 1,
          (int)computeLink.ThreadZ);
        computeBuffer = new DispComputeBuffer(outBuffer);
        diff = Time.realtimeSinceStartup - then;
        Debug.Log("Computing weights took " + diff + " seconds");

        // Get final compute result
        then = Time.realtimeSinceStartup;
        Debug.Log(computeBuffer.buffer.count + " values in the buffer");
        //computeData = new ComputeBufferStruct[mesh.triangles.Length / 3];
        var data = computeBuffer.GetData();
        //Debug.Log(data[0].id);
        computeBuffer.Dispose();

        // Reset values
        for (int i = 0, n = trianglesVisibility.Count; i < n; ++i)
        {
            trianglesVisibility[i] = false;
        }

        // Read values
        for (int i = 0, n = data.Length; i < n; ++i)
        {
            int id = data[i].id;
            if (id == 0) continue;
            id -= 1;
            trianglesVisibility[id] = true;
        }

        if(computeBuffer == null)
        {
            Debug.LogError("ERROR? ComputeBuffer is null");
            return;
        }
        diff = Time.realtimeSinceStartup - then;
        Debug.Log("Processing compute result took " + diff + " seconds");

        Debug.Log("End pre frame job");
    }

    void OnDrawGizmos()
    {
        if(mesh == null || !showPoints) return;
        for(int i = 0; i < trianglesPos.Count; ++i)
        {
            bool flag = trianglesVisibility[i];
            Vector3 pos = trianglesPos[i];
            if(flag) Gizmos.color = Color.green;
            else Gizmos.color = Color.red;
            Gizmos.DrawSphere(pos, samplesize);
        }
    }

}
