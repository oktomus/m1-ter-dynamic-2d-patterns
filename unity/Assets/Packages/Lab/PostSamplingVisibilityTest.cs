﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Rendering;

using GPUSampling;

/**
 * Sample a mesh
 * Then calculate the weight for each sample on a non 
 * animated triangulated mesh
 */
public class PostSamplingVisibilityTest : MonoBehaviour {

	[SerializeField] protected Mesh source;
	[SerializeField] protected ComputeShader shader;
	[SerializeField] protected int countByAxis = 32;
	[SerializeField] protected float sampleSize = 0.01f;
	[SerializeField] protected float maxdepth = 1.2f;
	[SerializeField] protected float samplingThresholdFactor = 0.2f;
	[SerializeField] protected bool showVoxels = false;
	[SerializeField] protected bool removeNeighbours = false;
	[SerializeField] protected bool showSamples = false;
	[SerializeField] protected bool calculateWeight = false;

	public ComputeBuffer shaderBuffer;

	public Material weightingMaterial;

	private class WeightedSample{
		public bool weight;
		public int triangle;
		public Vector3 position;
	};

	private Vector3 voxelSize = new Vector3();
	private List<Sample> samples = new List<Sample>();
	private List<WeightedSample> samplePositions = new List<WeightedSample>();

	void Start () {
		source.RecalculateBounds ();
		Bounds bounds = source.bounds;
		float BBSize = Mathf.Max (bounds.size.x, 
			Mathf.Max (bounds.size.y, bounds.size.z));

		float then = Time.realtimeSinceStartup;
		var buffer = GPUAABBSampler.Sample(shader, source, countByAxis);
		voxelSize = new Vector3(buffer.VoxelSize, buffer.VoxelSize, buffer.VoxelSize);
		var data = buffer.GetData();
		buffer.Dispose();

		for(int i = 0; i < data.Length; i++) {
			if(data[i].flag) {
				samples.Add(data[i]);
			}
		}

		float diff = Time.realtimeSinceStartup - then;
		Debug.Log ("GPU Sampling took " + diff + " seconds");
		Debug.Log ("Voxel count: " + samples.Count);

		// Barycentric to catersian
		then = Time.realtimeSinceStartup;

		PointOctree<AdjacentSample> samplesTree = new PointOctree<AdjacentSample>(
			BBSize, bounds.center, maxdepth);
		foreach(Sample s in samples) { // Create the initial list
			if (s.flag) {
				AdjacentSample sample = new AdjacentSample ();
				sample.b1 = s.b1;
				sample.b2 = s.b2;
				sample.trianglePos = s.trianglePos;
				sample.adjacentSamples = new List<AdjacentSample> ();
				sample.position = SampleForGPUData (source, s.trianglePos, s.b1, s.b2);
				samplesTree.Add(sample, sample.position);
			}
		}

		diff = Time.realtimeSinceStartup - then;
		Debug.Log ("Barycentric to cartesian & octree took " + diff + " seconds");

		float totalOctreeAcess = 0;
		float octreeAcessThen;

		if (removeNeighbours) {
            float threshold = buffer.VoxelSize * samplingThresholdFactor;
			then = Time.realtimeSinceStartup;	
			int removecount = 0;

			List<AdjacentSample> nonEmptySamples = new List<AdjacentSample> (); // The samples that have neighbourds

			List<AdjacentSample> allSamples = samplesTree.GetAll();
			for (int j = 0; j < allSamples.Count; j++) { // Process the set		
				AdjacentSample asample = allSamples [j];
				asample.adjacentsCentroid = new Vector3 ();
				// Thats where the octree is usefull
				octreeAcessThen = Time.realtimeSinceStartup;
				List<AdjacentSample> closeSamples = samplesTree.GetNearby(
					asample.position, threshold
				);
				totalOctreeAcess += Time.realtimeSinceStartup - octreeAcessThen;
				for (int i = 0; i < closeSamples.Count; i++) {
					AdjacentSample neighbour = closeSamples [i];
					if (neighbour != asample) {
						asample.adjacentSamples.Add (neighbour);
						asample.adjacentsCentroid += neighbour.position;
					}
				}
				// Process the centroid, if needed
				if (asample.adjacentSamples.Count > 0) {
					nonEmptySamples.Add (asample);	
					asample.adjacentsCentroid /= asample.adjacentSamples.Count;						
				}
			}

			diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Processing neighbour sets took " + diff + " seconds");
	
			then = Time.realtimeSinceStartup;		

			// For each point which have a non-empty set
			// Take the one which its position is the furthest from the centroid
			// Remove that point from the samples
			while (nonEmptySamples.Count > 0) {
				AdjacentSample candidate = null;
				float maxdist = -1.0f;
				for (int i = 0; i < nonEmptySamples.Count; i++) {
					AdjacentSample asample = nonEmptySamples [i];
					float dist = Vector3.Distance (asample.position, asample.adjacentsCentroid);
					if (dist > maxdist) {
						maxdist = dist;
						candidate = asample;
					}
				}

				nonEmptySamples.Remove (candidate);
				List<AdjacentSample> updateSamples = candidate.adjacentSamples;
				samplesTree.Remove(candidate);
				removecount++;

				// Update neighbourds (just the points that are of a distance less than voxel size)
				for (int i = 0; i < updateSamples.Count; i++) {
					AdjacentSample asample = updateSamples [i];
					if (asample.adjacentSamples.Remove (candidate)) {
						asample.adjacentsCentroid = new Vector3 ();
						for (int j = 0; j < asample.adjacentSamples.Count; j++) {
							asample.adjacentsCentroid += asample.adjacentSamples [j].position;
						}
						if (asample.adjacentSamples.Count > 0) {
							asample.adjacentsCentroid /= asample.adjacentSamples.Count;						
						} else {
							nonEmptySamples.Remove (asample);
						}
					}

				}
			}


			diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Removing " + removecount + " samples took " + diff + " seconds");
			Debug.Log ("New sample count " + samplesTree.Count);
			Debug.Log("Total Octree Access Time: " + totalOctreeAcess + " seconds");
		}

		List<AdjacentSample> finalSamples = samplesTree.GetAll();
		for(int i = 0; i < finalSamples.Count; i++) {
			WeightedSample s = new WeightedSample();
			s.weight = false;
			s.position = finalSamples[i].position;
			s.triangle = finalSamples[i].trianglePos;
			samplePositions.Add(s);
		}

	}

	private static Vector3 SampleForGPUData(Mesh mesh, int triangle, float a, float b)
	{
		Vector3 va = mesh.vertices[mesh.triangles[triangle]];
		Vector3 vb = mesh.vertices[mesh.triangles[triangle + 1]];
		Vector3 vc = mesh.vertices[mesh.triangles[triangle + 2]];
		return va + a * (vb - va) + b * (vc - va);
	}

/*
	void OnPostRender()
	{
		return;
		Debug.Log("On render image");
		if(weightingMaterial == null) 
		{
			Debug.LogError("No weighing material.");
			return;
		}
        //Graphics.ClearRandomWriteTargets();
        if(!weightingMaterial.SetPass(0))
		{
			Debug.LogError("Failed to set pass");
			return;
		}
        //weightingMaterial.SetInt("_width", Camera.main.pixelWidth);
        //weightingMaterial.SetInt("_height", Camera.main.pixelHeight);
        //weightingMaterial.SetBuffer("_myBuffer", shaderBuffer);
        //Graphics.SetRandomWriteTarget(1, shaderBuffer, true);
		//Graphics.DrawMeshNow(source, transform.position, transform.rotation);
        //Graphics.ClearRandomWriteTargets();

		//bool[] v = new bool[source.triangles.Length];
        //shaderBuffer.GetData(v);
		int countTrue = 0, countFalse = 0;
		//for(int i = 0; i < source.triangles.Length; ++i)
		//{
			//if(v[i]) countTrue++;
			//else countFalse++;
		//}
		Debug.Log(countTrue + " triangles visible");
		Debug.Log(countFalse + " triangles are not");

    }
	*/

	// Update is called once per frame
	void Update () {
        if (weightingMaterial == null)
        {
			Debug.LogError("No material assigned.");
        }
		if(shaderBuffer == null)
		{
			shaderBuffer = new ComputeBuffer(source.triangles.Length, Marshal.SizeOf(typeof(bool)), ComputeBufferType.Default);
			Graphics.SetRandomWriteTarget(1, shaderBuffer, true);
			bool[] bufferData = new bool[source.triangles.Length];
			for(int i = 0; i < source.triangles.Length; ++i)
			{
				bufferData[i] = false;
			}
			shaderBuffer.SetData(bufferData);
		}
		Debug.Log("On render image");
		if(weightingMaterial == null) 
		{
			Debug.LogError("No weighing material.");
			return;
		}
        //Graphics.ClearRandomWriteTargets();
        if(!weightingMaterial.SetPass(0))
		{
			Debug.LogError("Failed to set pass");
			return;
		}
		Graphics.DrawMeshNow(source, transform.position, transform.rotation);
		bool[] v = new bool[source.triangles.Length];
        shaderBuffer.GetData(v);
		int countTrue = 0, countFalse = 0;
		for(int i = 0; i < source.triangles.Length; ++i)
		{
            if (v[i]) countTrue++;
			else countFalse++;
		}
		Debug.Log(countTrue + " triangles visible");
		Debug.Log(countFalse + " triangles are not");

/* 
		if(false || calculateWeight){
			float then = Time.realtimeSinceStartup;
			Matrix4x4 MVMat = Camera.main.worldToCameraMatrix;
			Debug.Log(MVMat);
			MVMat = Matrix4x4.Inverse(MVMat);
			Debug.Log(MVMat);
			MVMat = Matrix4x4.Transpose(MVMat);
			Debug.Log(MVMat);
			// Weigh process
			for(int i = 0, n = samplePositions.Count; i < n; i++) {
				WeightedSample ws = samplePositions[i];
				ws.weight = false;
				int ita = source.triangles[ws.triangle];
				int itb = source.triangles[ws.triangle + 1];
				int itc = source.triangles[ws.triangle + 2];
				Vector3 va = source.normals[ita];
				Vector3 vb = source.normals[itb];
				Vector3 vc = source.normals[itc];
				Vector3 e1 = vc - va;
				Vector3 e2 = vb - va;
				Vector3 normal = Vector3.Normalize(Vector3.Cross(e1, e2));
				normal = MVMat * normal;
				if(Vector3.Dot(va - Camera.main.transform.position, normal) <= 0) ws.weight = false;
				else ws.weight = true;

			}
			float diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Calculating weights on CPU took " + diff + " seconds");
			calculateWeight = false;
		}

		*/
		
	}
	void OnDrawGizmos () {
		if(showVoxels) {
			Gizmos.color = Color.red;
			for(int v = 0; v < samples.Count; v++) {
				Gizmos.DrawCube(samples[v].position, voxelSize);
			}
		}
		if (showSamples) {
			for (int i = 0, n = samplePositions.Count; i < n; i++) {
				WeightedSample s = samplePositions[i];
				if(s.weight)  Gizmos.color = Color.yellow;
				else Gizmos.color = Color.blue;
				Gizmos.DrawSphere( s.position, sampleSize);
			}
		}
	}
}
