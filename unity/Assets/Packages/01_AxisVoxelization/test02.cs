﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GPUSampling;

/**
 * Test execution time and voxelization results on GPU
 * 
 * Results:
 *  For a mesh of 32530 verts, generating 102764 voxels takes less than a second
 *  GPU Instancing is used to display voxels. Be sure to set the instance as static.
 */

public class test02 : MonoBehaviour {

	[SerializeField] protected Mesh mesh;
	[SerializeField] protected ComputeShader voxelizer;
	[SerializeField] protected int count = 32;
	[SerializeField] protected GameObject voxelInstance;
	[SerializeField] protected bool showVoxels = false;

	// Use this for initialization
	void Start () {
		float then = Time.realtimeSinceStartup;
		var data = GPUVoxelizer.Voxelize(voxelizer, mesh, count);
		//GetComponent<MeshFilter>().sharedMesh = GPUVoxelizer.Build(
		/*Vector3 vecsize = new Vector3 (size, size, size);
		foreach (Voxel voxel in data) {
			GameObject b = Instantiate (voxelInstance, voxel.position, new Quaternion ());
			b.transform.localScale = vecsize;
			b.transform.parent = transform;
			voxelsInstance.Add (b);
		}*/

		var unit = data.VoxelSize;

		var voxels = data.GetVoxels	();
		float now = Time.realtimeSinceStartup;
		float diff = now - then;
		Debug.Log ("GPU Voxelization took " + diff + " seconds");
		Debug.Log ("Voxels count: " + voxels.Length);
		then = Time.realtimeSinceStartup;
		if (showVoxels) {
			Vector3 vecsize = new Vector3 (unit, unit, unit);
			for (int i = 0, n = voxels.Length; i < n; i++) {
				var v = voxels [i];
				if (v.flag) {
					var center = v.position;

					GameObject b = Instantiate (voxelInstance, center, new Quaternion ());
					b.transform.localScale = vecsize;
					b.transform.parent = transform;

				}
			}
		}
		now = Time.realtimeSinceStartup;
		diff = now - then;
		Debug.Log ("Instancing Voxels took " + diff + " seconds");
	

		data.Dispose();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
