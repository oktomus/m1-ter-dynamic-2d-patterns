﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GPUSampling;

/**
 * Test execution time and sampling results on GPU
 * 
 */

public class test03 : MonoBehaviour {

	[SerializeField] protected Mesh mesh;
	[SerializeField] protected ComputeShader shader;
	[SerializeField] protected int count = 32;
	[SerializeField] protected GameObject voxelInstance;
	[SerializeField] protected bool showVoxels = false;
	[SerializeField] protected bool showSamples = false;
	[SerializeField] protected bool removeNeighbours = false;
	[SerializeField] protected float sampleSize = 0.01f;
	[SerializeField] protected float samplingThresholdFactor = 0.2f;
	private List<Sample> samples = new List<Sample> ();
	private List<Vector3> samplesDebug = new List<Vector3> ();
	private float unit;

	// Use this for initialization
	void Start () {
		/*
		 * STRUCT = COPY
		 * CLASS = REFERENCE
		AdjacentSample test = new AdjacentSample ();
		test.b1 = 2.0f;
		test.b2 = 0.0f;
		List<AdjacentSample> listTest = new List<AdjacentSample> ();
		listTest.Add (test);
		test.b2 = 1.0f;
		test.b1 = 5.0f;
		Debug.Log ("Object coord : " + test.b1 + ", " + test.b2);
		Debug.Log ("List Object coord : " + listTest [0].b1 + ", " + listTest [0].b2);
		*/

		float then = Time.realtimeSinceStartup;
		var data = GPUSampler.Voxelize(shader, mesh, count);


		unit = data.VoxelSize;
		float threshold = unit * samplingThresholdFactor;

		// Remove empty blocks
		samples = new List<Sample> ( data.GetData());
		data.Dispose ();
		// samples.RemoveAll (p => !p.flag); not yet

	

		float diff = Time.realtimeSinceStartup - then;
		Debug.Log ("GPU Sampling took " + diff + " seconds");
		Debug.Log ("Voxel count: " + samples.Count);

		// Remove samples to close to each other
		// Step 3 of Stratified Point Sampling of 3D Models
		then = Time.realtimeSinceStartup;

		// For each point, define the set of points that have a distance of less than the size of a voxel
		// And calculate the centroid of this set
		List<AdjacentSample> editSamples = new List<AdjacentSample>();
		foreach(Sample s in samples) { // Create the initial list
			if (s.flag) {
				AdjacentSample sample = new AdjacentSample ();
				sample.b1 = s.b1;
				sample.b2 = s.b2;
				sample.trianglePos = s.trianglePos;
				sample.adjacentSamples = new List<AdjacentSample> ();
				sample.position = SampleForGPUData (mesh, s.trianglePos, s.b1, s.b2);
				editSamples.Add (sample);
			}
		}

		diff = Time.realtimeSinceStartup - then;
		Debug.Log ("Barycentric to Cartesian took " + diff + " seconds");

		if (removeNeighbours) {
			then = Time.realtimeSinceStartup;	
			int removecount = 0;

			List<AdjacentSample> nonEmptySamples = new List<AdjacentSample> (); // The samples that have neighbourds

			for (int j = 0; j < editSamples.Count; j++) { // Process the set		
				AdjacentSample asample = editSamples [j];
				asample.adjacentsCentroid = new Vector3 ();
				// Thats where the octree is usefull
				// Unless done on gpu
				for (int i = 0; i < editSamples.Count; i++) {
					if (i != j) {
						AdjacentSample neighbour = editSamples [i];
						if (Vector3.Distance (asample.position, neighbour.position) < threshold) {
							asample.adjacentSamples.Add (neighbour);
							asample.adjacentsCentroid += neighbour.position;
						}
					}
				}
				// Process the centroid, if needed
				if (asample.adjacentSamples.Count > 0) {
					nonEmptySamples.Add (asample);	
					asample.adjacentsCentroid /= asample.adjacentSamples.Count;						
				}
			}

			diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Processing neighbour sets took " + diff + " seconds");
	
			then = Time.realtimeSinceStartup;		

			// For each point which have a non-empty set
			// Take the one which its position is the furthest from the centroid
			// Remove that point from the samples
			while (nonEmptySamples.Count > 0) {
				AdjacentSample candidate = null;
				float maxdist = -1.0f;
				for (int i = 0; i < nonEmptySamples.Count; i++) {
					AdjacentSample asample = nonEmptySamples [i];
					float dist = Vector3.Distance (asample.position, asample.adjacentsCentroid);
					if (dist > maxdist) {
						maxdist = dist;
						candidate = asample;
					}
				}

				nonEmptySamples.Remove (candidate);
				List<AdjacentSample> updateSamples = candidate.adjacentSamples;
				editSamples.Remove(candidate);
				removecount++;

				// Update neighbourds (just the points that are of a distance less than voxel size)
				for (int i = 0; i < updateSamples.Count; i++) {
					AdjacentSample asample = updateSamples [i];
					if (asample.adjacentSamples.Remove (candidate)) {
						asample.adjacentsCentroid = new Vector3 ();
						for (int j = 0; j < asample.adjacentSamples.Count; j++) {
							asample.adjacentsCentroid += asample.adjacentSamples [j].position;
						}
						if (asample.adjacentSamples.Count > 0) {
							asample.adjacentsCentroid /= asample.adjacentSamples.Count;						
						} else {
							nonEmptySamples.Remove (asample);
						}
					}

				}
			}


			diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Removing " + removecount + " samples took " + diff + " seconds");
			Debug.Log ("New sample count " + editSamples.Count);
		}

		if (showSamples) {
			for (int i = 0, n = editSamples.Count; i < n; i++) {
				samplesDebug.Add (editSamples [i].position);
			}

		}

		if (showVoxels) {
			then = Time.realtimeSinceStartup;

			Vector3 vecsize = new Vector3 (unit, unit, unit);
			for (int i = 0, n = samples.Count; i < n; i++) {
				var v = samples [i];
				if (v.flag) {
					var center = v.position;

					GameObject b = Instantiate (voxelInstance, center, new Quaternion ());
					b.transform.localScale = vecsize;
					b.transform.parent = transform;

				}
			}
			diff = Time.realtimeSinceStartup - then;
			Debug.Log ("Instancing Voxels took " + diff + " seconds");
		}


	}

	private static Vector3 SampleForGPUData(Mesh mesh, int triangle, float a, float b)
	{
		Vector3 va = mesh.vertices[mesh.triangles[triangle]];
		Vector3 vb = mesh.vertices[mesh.triangles[triangle + 1]];
		Vector3 vc = mesh.vertices[mesh.triangles[triangle + 2]];
		return va + a * (vb - va) + b * (vc - va);
	}

	private void OnDrawGizmos () {
		if (showSamples) {
			Gizmos.color = Color.red;
			for (int i = 0, n = samplesDebug.Count; i < n; i++) {
				Gizmos.DrawSphere( samplesDebug [i], sampleSize);
			}
		}

	}

	// Update is called once per frame
	void Update () {

	}
}
