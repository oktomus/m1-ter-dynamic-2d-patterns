﻿using System;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * Represent a shader compute function
	 */
	public class Kernel
	{
		/**
		 * The index of the kernel (function) in the shader
		 */
		public int Index { get { return index; } }

		/**
		 * Thread size
		 */
		public uint ThreadX { get { return threadX; } }
		public uint ThreadY { get { return threadY; } }
		public uint ThreadZ { get { return threadZ; } }

		private int index;
		private uint threadX, threadY, threadZ;

		public Kernel (ComputeShader shader, string key)
		{
			index = shader.FindKernel (key);
			if (index < 0) {
				Debug.LogError ("Kernel not found. Key: " + key);
				return;
			}

			// Query work group size of compute
			shader.GetKernelThreadGroupSizes (index,
				out threadX,
				out threadY,
				out threadZ);
		}
	}
}

