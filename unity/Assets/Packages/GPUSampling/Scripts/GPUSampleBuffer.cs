﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * A sample GPU buffer associated to a shader compute buffer
	 */
	public class GPUSampleBuffer : System.IDisposable
	{

		/**
		 * The shader compute buffer in which the samples are being used
		 */
		public ComputeBuffer CBuffer { get { return buffer; } }

		/**
		 * The size of the voxel
		 */
		public float VoxelSize { get { return size; } }

		private float size;
		private ComputeBuffer buffer;

		public GPUSampleBuffer (ComputeBuffer buf, float s)
		{
			buffer = buf;
			size = s;
		}

		/**
		 * Returns the samples of the GPU buffer
		 */
		public Sample[] GetData() {
			var data = new Sample[CBuffer.count];
			CBuffer.GetData (data);
			return data;
		}

		/**
		 * Release the GPU Buffer
		 */
		public void Dispose() {
			buffer.Release ();
		}

	}
}

