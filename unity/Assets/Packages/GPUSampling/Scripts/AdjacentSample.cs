﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * One sample that can have neighbours
	 */
	public class AdjacentSample {
		public float b1;
		public float b2;
		public Vector3 position;
		public int trianglePos;
		public List<AdjacentSample> adjacentSamples;
		public Vector3 adjacentsCentroid;
	}
}

