﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Rendering;

namespace GPUSampling
{
	/**
	 * Wrapper for computing the sampling on GPU
	 */
	public class GPUSampler
	{

		/**
		 * Shader variables/kernel names
		 */
		protected const string kComputeKey = "Sampler";
		protected const string kStartKey = "_Start", kEndKey = "_End", kSizeKey = "_Size";
		protected const string kUnitKey = "_Unit", kHalfUnitKey = "_HalfUnit";
		protected const string kWidthKey = "_Width", kHeightKey = "_Height", kDepthKey = "_Depth";
		protected const string kTriCountKey = "_TrianglesCount";
		protected const string kVertBufferKey = "_VertBuffer", kTriBufferKey = "_TriBuffer";
		protected const string kOutputBufferKey = "_SampleBuffer";


		public static GPUSampleBuffer Voxelize(
			ComputeShader shader,
			Mesh source,
			int countByAxis = 32) {

			// Pre process
			source.RecalculateBounds ();

			// Size utils
			Bounds bounds = source.bounds;
			float BBSize = Mathf.Max (bounds.size.x, 
				Mathf.Max (bounds.size.y, bounds.size.z));
			float VSize = BBSize / countByAxis;

			// Buffer setup
			ComputeBuffer verticesBuffer = new ComputeBuffer (source.vertices.Length,
				Marshal.SizeOf (typeof(Vector3)));
			verticesBuffer.SetData (source.vertices);

			ComputeBuffer trisBuffer = new ComputeBuffer (source.triangles.Length,
				Marshal.SizeOf (typeof(int)));
			trisBuffer.SetData (source.triangles);

			ComputeBuffer outputBuffer = new ComputeBuffer (
				// Number of voxel on each axis
				Mathf.CeilToInt ((bounds.size.x / BBSize * countByAxis)) *
				Mathf.CeilToInt ((bounds.size.y / BBSize * countByAxis)) *
				Mathf.CeilToInt ((bounds.size.z / BBSize * countByAxis)),
				Marshal.SizeOf (typeof(Sample))
			);

			// Compute function link
			Kernel kernel = new Kernel(shader, kComputeKey);

			// Map bounds on GPU
			shader.SetVector(kStartKey, bounds.min);
			shader.SetVector(kEndKey, bounds.max);
			shader.SetVector(kSizeKey, bounds.size);

			Vector3 realSize = bounds.size;
			int width = Mathf.CeilToInt (realSize.x / VSize);
			int height = Mathf.CeilToInt (realSize.y / VSize);
			int depth= Mathf.CeilToInt (realSize.z / VSize);

			shader.SetFloat (kUnitKey, VSize);
			shader.SetFloat (kHalfUnitKey, VSize * 0.5f);
			shader.SetInt (kWidthKey, width);
			shader.SetInt (kHeightKey, height);
			shader.SetInt (kDepthKey, depth);

			// Map mesh on GPU
			shader.SetBuffer(kernel.Index, kVertBufferKey, verticesBuffer);
			shader.SetInt (kTriCountKey, trisBuffer.count);
			shader.SetBuffer (kernel.Index, kTriBufferKey, trisBuffer);
			shader.SetBuffer (kernel.Index, kOutputBufferKey, outputBuffer);

			// GO
			shader.Dispatch (kernel.Index, 
				width / (int)kernel.ThreadX + 1,
				height / (int)kernel.ThreadY + 1,
				(int)kernel.ThreadZ);

			// Free
			verticesBuffer.Dispose();
			trisBuffer.Dispose ();

			return new GPUSampleBuffer (outputBuffer, VSize);
		}

	}
}

