﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * A voxel GPU buffer associated to a shader compute buffer
	 */
	public class GPUVoxels : System.IDisposable
	{

		/**
		 * The shader compute buffer in which the voxel is used
		 */
		public ComputeBuffer CBuffer { get { return buffer; } }

		/**
		 * The size of the voxel
		 */
		public float VoxelSize { get { return size; } }

		private float size;
		private ComputeBuffer buffer;
		
		public GPUVoxels (ComputeBuffer buf, float s)
		{
			buffer = buf;
			size = s;
		}

		/**
		 * Returns the voxels of the GPU buffer
		 */
		public Voxel[] GetVoxels() {
			var voxels = new Voxel[CBuffer.count];
			CBuffer.GetData (voxels);
			return voxels;
		}

		/**
		 * Release the GPU Buffer
		 */
		public void Dispose() {
			buffer.Release ();
		}

	}
}

