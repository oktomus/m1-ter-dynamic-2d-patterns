﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * One sample in a voxel grid
	 */
	public struct Sample {
		public Vector3 position;
		public bool flag;
		public float b1;
		public float b2;
		public int trianglePos;
	}
}

