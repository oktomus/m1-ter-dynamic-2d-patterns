﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPUSampling
{
	/**
	 * One voxel
	 */
	public struct Voxel {
		public Vector3 position;
		public bool flag;
	}
}

