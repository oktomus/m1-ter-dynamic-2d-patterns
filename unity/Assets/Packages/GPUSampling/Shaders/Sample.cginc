﻿#ifndef __SAMPLE_COMMON_INCLUDED__

#define __SAMPLE_COMMON_INCLUDED__

struct Sample {
	float3 position; // Voxel position
	bool flag; // Voxel state
	float b1; // Position on the surface, if any
	float b2; // Exprimed as barycentric
	int trianglePos; // The id of the first vertex id in the mesh triangle list
};

#endif