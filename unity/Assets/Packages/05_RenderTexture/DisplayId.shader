Shader "Unlit/DisplayPrimitveId"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Cull Back
			ZWrite On
			ZTest LEqual
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}

			fixed4 frag (v2f i, uint pid : SV_PrimitiveID) : SV_Target
			{
				// sample the texture
				float id = (1.0f/ 1526.0f) * pid;
				fixed4 col = fixed4(id, id, id, 1.0);
				return col;
			}
			ENDCG
		}
	}
}