﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine.Rendering;

public class ShaderBufferTest : MonoBehaviour {

	public Material renderingMaterial;
	private ComputeBuffer shaderBuffer;
    public Material renderTextureDebuger;

    public bool render;
    public bool continuous;

	public Mesh source;

    public float resolution = 0.01f;

    private RenderTexture rtex;

    public int depth = 24;
	
	// Use this for initialization
	void Start () {

    }

    void Update()
    {
        if(render)
        {
            // Try to render manually
            Display mainDisp = Display.main;
            int width = mainDisp.renderingWidth,
                height = mainDisp.renderingHeight;
                //depth = mainDisp.depthBuffer.depth;
            //if(rtex != null) rtex.Release();
            RenderTexture rt = UnityEngine.RenderTexture.active;
            UnityEngine.RenderTexture.active = rtex;
            GL.Clear(true, true, Color.clear);
            UnityEngine.RenderTexture.active = rt; 
            if(!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBInt))
            {
                Debug.LogError("Texture format not supported");
                return;
            }
            if(rtex == null)
                rtex = new RenderTexture((int)(width * resolution), (int)(height * resolution), depth, RenderTextureFormat.ARGB32);
            Debug.Log("Resolution: " + (int) (width * resolution) + "x" + (int) (height * resolution));
            if(renderTextureDebuger != null)
            {
                renderTextureDebuger.mainTexture = rtex;
            }

            Graphics.SetRenderTarget(rtex);
            if (!renderingMaterial.SetPass(1))
            {
                Debug.LogError("Failed to set pass");
                return;
            }
            Matrix4x4 P = GL.GetGPUProjectionMatrix(Camera.main.projectionMatrix, true);
            Matrix4x4 V = Camera.main.worldToCameraMatrix;
            Matrix4x4 M = transform.localToWorldMatrix;
            renderingMaterial.SetMatrix("_MATRIX_M", M);
            renderingMaterial.SetMatrix("_MATRIX_V", V);
            renderingMaterial.SetMatrix("_MATRIX_P", P);
            //Matrix4x4 MVP = P * V * M;
            //renderingMaterial.SetMatrix("_MATRIX_MVP", MVP);
            Graphics.DrawMeshNow(source, transform.position, transform.rotation);
            Debug.Log("Rendering finished.");
            Debug.Log("Try reading the texture");
            //render = false;

            // Read texture

            float then = Time.realtimeSinceStartup;
            Texture2D tex = new Texture2D(rtex.width, rtex.height);
            tex.ReadPixels(new Rect(0, 0, rtex.width, rtex.height), 0, 0);
            tex.Apply();
            float diff = Time.realtimeSinceStartup - then;
            Debug.Log("Transfering render texture to texture readable took " + diff + " seconds");
            then = Time.realtimeSinceStartup;
            HashSet<String> visibleTriangles = new HashSet<String>();
            Color[] pixels = tex.GetPixels();
            Debug.Log(pixels[0].ToString());
            for(int i = 0; i < pixels.Length; i++)
            {
                //visibleTriangles.Add(
                    //((int) (pixels[i].r * 1526)).ToString());
                if(pixels[i].r == 0) continue;
                visibleTriangles.Add(
                    ((int) (pixels[i].r) - 1).ToString());
            }
            String[] visibleTrianglesDebug = new String[visibleTriangles.Count];
            visibleTriangles.CopyTo(visibleTrianglesDebug);
            Debug.Log("Visible triangles: " + String.Join(", ", visibleTrianglesDebug));
            diff = Time.realtimeSinceStartup - then;
            Debug.Log("Copying render texture took " + diff + " seconds");

            // Read buffer

            if(!continuous) render = false;
        }
		
	}
}
