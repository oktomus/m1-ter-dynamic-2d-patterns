﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/Weight"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Pass {
            Color (0, 0, 0, 0)
        }

		Pass
		{
			Cull Back
			ZWrite On
			ZTest LEqual
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
			};

			float4x4 _MATRIX_M;
			float4x4 _MATRIX_V;
			float4x4 _MATRIX_P;
			float4x4 _MATRIX_MVP;

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				//fixed4 color : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _triangleCount;
			
			v2f vert (appdata v)
			{
				v2f o;
				//o.vertex = UnityObjectToClipPos(v.vertex);
				//o.vertex = mul(_MATRIX_P, mul(_MATRIX_V, mul(_MATRIX_M, v.vertex)));
				o.vertex = mul(_MATRIX_MVP, v.vertex);

				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			//int _count;
			//RWStructuredBuffer<bool> _myBuffer : register(u1);
			
			fixed4 frag (v2f i, uint pid : SV_PrimitiveID) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				//fixed4 col = fixed4(1, 1, 1, 1);
				float id = (1.0f / _triangleCount) * pid;
				//discard;
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return fixed4(id, 0, 0, 1);
				//return int4(1, 0, 0, 1);
			}
			ENDCG
		}
	}
}
