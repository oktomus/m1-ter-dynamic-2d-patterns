#pragma kernel ComputeSampling

#include "UnityCG.cginc"

struct Voxel {
    bool flag;
    float3 position;
	//float t1; 
	//float t2; 
    uint triangle_id;
};

#define THREAD_X 8
#define THREAD_Y 8
#define THREAD_Z 1

CBUFFER_START(SampleParams)

float3 _Start, _End, _Size;
float _Unit, _HalfUnit;

int _Width, _Height, _Depth;
int _SamplingMaxIterations;
float _SamplingDistanceThreshold; // Phy * Voxel Edge Length

CBUFFER_END

StructuredBuffer<float3> _VertBuffer;
StructuredBuffer<int> _TriBuffer;
int _TrianglesCount;

RWStructuredBuffer<Voxel> _VoxelBuffer;

// RANDOM Number generation
// See http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/

uint rng_state;

uint rand_lcg()
{
    // LCG values from Numerical Recipes
    rng_state = 1664525 * rng_state + 1013904223;
    return rng_state;
}

uint rand_xorshift()
{
    // Xorshift algorithm from George Marsaglia's paper
    rng_state ^= (rng_state << 13);
    rng_state ^= (rng_state >> 17);
    rng_state ^= (rng_state << 5);
    return rng_state;
}

int is_point_in_voxel(float3 voxel, float3 pos) {
	if(pos.x < voxel.x - _HalfUnit || pos.x > voxel.x + _HalfUnit) return -1;
	if(pos.y < voxel.y - _HalfUnit || pos.y > voxel.y + _HalfUnit) return -1;
	if(pos.z < voxel.z - _HalfUnit || pos.z > voxel.z + _HalfUnit) return -1;
	return 1;
}

float2 uniform_sample(float3 voxel, float3 a, float3 b, float3 c) {
	float3 sample;
	float3 ba = b - a;
	float3 ca = c - a;

	float2 bary;
    float distance;

    bool generated = false;
	int iter = 0;
	while(!generated && iter < _SamplingMaxIterations) {
		// Generate uniform sample
		// See http://mathworld.wolfram.com/TrianglePointPicking.html
		// And https://math.stackexchange.com/questions/538458/triangle-point-picking-in-3d#538472
		bary.x = float(rand_xorshift()) * (1.0 / 4294967296.0);
		bary.y = float(rand_xorshift()) * (1.0 / 4294967296.0);

		if (bary.x + bary.y > 1.0f) {
			bary.x = 1 - bary.x;
			bary.y = 1 - bary.y;
		}
		sample = a + bary.x * ba + bary.y * ca;

		//inside = is_point_in_voxel(voxel, sample);
        // Test distance threshold
        sample = voxel - sample;
        sample *= sample;
        distance = sqrt(sample.x + sample.y + sample.z);

        if (distance <= _HalfUnit)
            generated = true;

        ++iter;
	}

	if (!generated) { // No distribution matches correctly
		// Then maybe the triangle intersection was wrong
		// We should ignore it
		bary.x = -2.0f;
		bary.y = -2.0f;
	}

	return bary;	

}

bool planeBoxOverlap(float3 normal, float3 vertex, float3 box) {
    float3 vmin, vmax;

    // X
    if (normal.x > 0.0f) {
        vmin.x = -box.x - vertex.x;
        vmax.x = box.x - vertex.x;
    } else { 
        vmin.x = box.x - vertex.x;
        vmax.x = -box.x - vertex.x;
    }
    // Y
    if (normal.y > 0.0f) {
        vmin.y = -box.y - vertex.y;
        vmax.y = box.y - vertex.y;
    } else { 
        vmin.y = box.y - vertex.y;
        vmax.y = -box.y - vertex.y;
    }
    // Z
    if (normal.z > 0.0f) {
        vmin.z = -box.z - vertex.z;
        vmax.z = box.z - vertex.z;
    } else { 
        vmin.z = box.z - vertex.z;
        vmax.z = -box.z - vertex.z;
    }

    if (dot (normal, vmin) > 0.0f)
        return false;
    if (dot (normal, vmax) >= 0.0f)
        return true;

    return false;
}

/*======================== X-tests ========================*/
#define AXISTEST_X01(a, b, fa, fb)			   \
	p0 = a*va.y - b*va.z;			       	   \
	p2 = a*vc.y - b*vc.z;			       	   \
    if(p0<p2) {fmin=p0; fmax=p2;} else {fmin=p2; fmax=p0;} \
	rad = fa * boxhsize.y + fb * boxhsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_X2(a, b, fa, fb)			   \
	p0 = a*va.y - b*va.z;			           \
	p1 = a*vb.y - b*vb.z;			       	   \
    if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhsize.y + fb * boxhsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

/*======================== Y-tests ========================*/
#define AXISTEST_Y02(a, b, fa, fb)			   \
	p0 = -a*va.x + b*va.z;		      	   \
	p2 = -a*vc.x + b*vc.z;	       	       	   \
    if(p0<p2) {fmin=p0; fmax=p2;} else {fmin=p2; fmax=p0;} \
	rad = fa * boxhsize.x + fb * boxhsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_Y1(a, b, fa, fb)			   \
	p0 = -a*va.x + b*va.z;		      	   \
	p1 = -a*vb.x + b*vb.z;	     	       	   \
    if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhsize.x + fb * boxhsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

/*======================== Z-tests ========================*/
#define AXISTEST_Z12(a, b, fa, fb)			   \
	p1 = a*vb.x - b*vb.y;			           \
	p2 = a*vc.x - b*vc.y;			       	   \
    if(p2<p1) {fmin=p2; fmax=p1;} else {fmin=p1; fmax=p2;} \
	rad = fa * boxhsize.x + fb * boxhsize.y;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_Z0(a, b, fa, fb)			   \
	p0 = a*va.x - b*va.y;				   \
	p1 = a*vb.x - b*vb.y;			           \
    if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhsize.x + fb * boxhsize.y;   \
	if(fmin>rad || fmax<-rad) return false;

bool triBoxOverlap(float3 boxcenter, float3 boxhsize, float3 va, float3 vb, float3 vc) {

    float p0, p1, p2, rad, fmin, fmax;

    // Translate triangle so that boxcenter is at 0,0,0
    va -= boxcenter;
    vb -= boxcenter;
    vc -= boxcenter;

    // Compute edges
    float3 e0 = vb - va;
    float3 e1 = vc - vb;
    float3 e2 = va - vc;


    // Crossproduct test
    float fex = abs(e0.x);
    float fey = abs(e0.y);
    float fez = abs(e0.z);

    // Go Real test with edges

    //    AXISTEST_X01(e0[Z], e0[Y], fez, fey);
    AXISTEST_X01(e0.z, e0.y, fez, fey)
    //    AXISTEST_Y02(e0[Z], e0[X], fez, fex);
    AXISTEST_Y02(e0.z, e0.x, fez, fex)
    //    AXISTEST_Z12(e0[Y], e0[X], fey, fex);
    AXISTEST_Z12(e0.y, e0.x, fey, fex)

    fex = abs(e1.x);
    fey = abs(e1.y);
    fez = abs(e1.z);
    // AXISTEST_X01(e1[Z], e1[Y], fez, fey);
    AXISTEST_X01(e1.z, e1.y, fez, fey)
    // AXISTEST_Y02(e1[Z], e1[X], fez, fex);
    AXISTEST_Y02(e1.z, e1.x, fez, fex)
    // AXISTEST_Z0(e1[Y], e1[X], fey, fex);
    AXISTEST_Z0(e1.y, e1.x, fey, fex)

    fex = abs(e2.x);
    fey = abs(e2.y);
    fez = abs(e2.z);
    //    AXISTEST_X2(e2[Z], e2[Y], fez, fey);
    AXISTEST_X2(e2.z, e2.y, fez, fey)
    // AXISTEST_Y1(e2[Z], e2[X], fez, fex);
    AXISTEST_Y1(e2.z, e2.x, fez, fex)
    // AXISTEST_Z12(e2[Y], e2[X], fey, fex);
    AXISTEST_Z12(e2.y, e2.x, fey, fex)

    // End test 01
    /*  first test overlap in the {x,y,z}-directions */
    /*  find min, max of the triangle each direction, and test for overlap in */
    /*  that direction -- this is equivalent to testing a minimal AABB around */
    /*  the triangle against the AABB */

    // X TEST
    fmin = min(va.x, min(vb.x, vc.x));
    fmax = max(va.x, max(vb.x, vc.x));
    if(fmin > boxhsize.x || fmax < -boxhsize.x) return false;

    // Y TEST
    fmin = min(va.y, min(vb.y, vc.y));
    fmax = max(va.y, max(vb.y, vc.y));
    if(fmin > boxhsize.y || fmax < -boxhsize.y) return false;

    // Z TEST
    fmin = min(va.z, min(vb.z, vc.z));
    fmax = max(va.z, max(vb.z, vc.z));
    if(fmin > boxhsize.z || fmax < -boxhsize.z) return false;

    /*  test if the box intersects the plane of the triangle */
    /*  compute plane equation of triangle: normal*x+d=0 */
    float3 normal = cross(e0, e1);
    if(!planeBoxOverlap(normal, va, boxhsize)) return false;

    return true;
}

int get_voxel_index(int x, int y, int z) {
    return z * (_Width * _Height) + y * _Width + x;
}

float3 get_voxel_position(int x, int y, int z) {
    return float3(
        _Start.x + _Unit * x + _HalfUnit,
        _Start.y + _Unit * y + _HalfUnit,
        _Start.z + _Unit * z + _HalfUnit
    );
}

float tri_hit(float3 ori, float3 dir, float3 a, float3 b, float3 c) {
    float3 e1 = b - a;
    float3 e2 = c - a;
    float3 P = cross(dir, e2);
    float det = dot(e1, P);

    const float epsilon = 0.001;

    // if (det > -epsilon && det < epsilon) return false;
    if(abs(det) < epsilon) return -1;

    float invDet = 1.0 / det;

    float3 T = ori - a;
    float u = dot(T, P) * invDet;
    if (u < 0.0 || u > 1.0) return -1;

    float3 Q = cross(T, e1);
    float v = dot(dir, Q * invDet);
    if (v < 0.0 || u + v > 1.0) return -1;

    float t = dot(e2, Q) * invDet;
    if(t > epsilon) {
        return t;
    }

    return -1.0;
}

/**
 * Throw a ray from the 3 axes planes 
 * Plane X, Y  :   o
 * Plane Y, Z  : <-
 * Plane X, Z  :   v
 */
void axis_voxelisation(int x, int y, int z)
{
    float3 ray_origin, dir;
    
    int plane_mode = 0;
    if (z == 0)
    {
        plane_mode = 1;
        ray_origin = float3(
            _Start.x + _Unit * (float)x - _HalfUnit,
            _Start.y + _Unit * (float)y - _HalfUnit,
            _Start.z - _Unit
        );
        dir = float3(0, 0, 1);
    }
    else if (y == 0)
    {
        plane_mode = 2;
        ray_origin = float3(
            _Start.x + _Unit * (float)x - _HalfUnit,
            _Start.y - _Unit,
            _Start.z + _Unit * (float)z - _HalfUnit
        );
        dir = float3(0, 1, 0);
    }
    else
    {
        plane_mode = 3;
        ray_origin = float3(
            _Start.x - _Unit,
            _Start.y + _Unit * (float)y - _HalfUnit,
            _Start.z + _Unit * (float)z - _HalfUnit
        );
        dir = float3(1, 0, 0);
    }

    uint three = 3;
    float3 voxel_position;
    uint vid;
    float3 boxhsize;
    boxhsize.x = _HalfUnit;
    boxhsize.y = _HalfUnit;
    boxhsize.z = _HalfUnit;

    float extension = 1.0f + _HalfUnit;

    // check intersections
    for(int i = 0; i < _TrianglesCount; i += 3) {
        int a = _TriBuffer[i];
        int b = _TriBuffer[i + 1];
        int c = _TriBuffer[i + 2];

        float3 v0 = _VertBuffer[a];
        float3 v1 = _VertBuffer[b];
        float3 v2 = _VertBuffer[c];

        float3 ab = v1 - v0;
        float3 ca = v0 - v2;
        float3 bc = v2 - v1;
        // Conservative rasterization
        //v1 += ab * extension;
        //v0 += ca * extension;
        //v2 += bc * extension;

        float dist = tri_hit(ray_origin, dir, v0, v1, v2);
        if(dist >= 0.0) {
            if (plane_mode == 1)
                z = dist / _Unit;
            else if (plane_mode == 2)
                y = dist / _Unit;
            else
                x = dist / _Unit;

            voxel_position = get_voxel_position(x, y, z);
            vid = get_voxel_index(x, y, z);

            // Sample
            //float2 bary = uniform_sample(voxel_position, v0, v1, v2);
            // If sampling failed
            //if(bary.x == -2.0f && bary.y == -2.0f) continue;
            float3 sample_position = ray_origin + dir * dist;

            //if (distance(sample_position, voxel_position) > _Unit) continue;

            Voxel voxel = _VoxelBuffer[vid];
            if (!voxel.flag || distance(sample_position, voxel_position) < distance(voxel.position, voxel_position))
            {
                voxel.position = sample_position;
                voxel.flag = true;
                voxel.triangle_id = i / three;
                _VoxelBuffer[vid] = voxel;
            }

            // Initiate sample
            //voxel.t1 = bary.x;
            //voxel.t2 = bary.y;
        }
    }

}

[numthreads(THREAD_X, THREAD_Y, THREAD_Z)]
void ComputeSampling (uint3 id : SV_DispatchThreadID)
{
	rng_state = id.x + id.y + id.z ;
    int x = (int)id.x;
    int y = (int)id.y;
    int z = (int)id.z;

    if (z != 1) return;

    if (x < _Width && y < _Height) axis_voxelisation(x, y, 0);
    if (y < _Depth) axis_voxelisation(x, 0, y);
    if (x < _Depth && y < _Depth) axis_voxelisation(0, y, x);
}
