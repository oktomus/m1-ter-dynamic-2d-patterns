﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent (typeof(MeshFilter), typeof(Renderer))]
public class ComputeTransformsTest : MonoBehaviour, IPreFrameJob
{

    // Util class to debug time
    private class TimeLogger
    {
        private float then;

        public void start()
        {
            then = Time.realtimeSinceStartup;
        }

        public void log(String message)
        {
            Debug.Log(message + (Time.realtimeSinceStartup - then) + "s");
        }
    }

    // SAMPLING RELATED 
    private class Sample
    {
        public Vector3 position;
        public uint triangle_id;
        public float cweight; // Weight on the current frame 
        public float pweight; // Weight on the current frame 
        public Vector2 ci; // Image space location of the sample minus current centroid
        public Vector2 pi; // Image space location in previous frame minus previous centroid
        public Vector2 pimage_location;
    }

    private class SampleNode
    {
        public Sample sample;
        public List<SampleNode> neighbours;
        public Vector3 centroid;
    }

    private struct Voxel
    {
        public bool flag;
        public Vector3 position;
        // Barycentric coordinate
        //public float t1; // A + t1 * AB
        //public float t2; // A + t2 * AC
        public int triangle_id;
    }

	private class Kernel
	{
		public int Index { get { return index; } }
		public uint ThreadX { get { return threadX; } }
		public uint ThreadY { get { return threadY; } }
		public uint ThreadZ { get { return threadZ; } }

		int index;
		uint threadX, threadY, threadZ;

		public Kernel(ComputeShader shader, string key)
		{
			index = shader.FindKernel(key);
			if (index < 0)
			{
				Debug.LogWarning("Can't find kernel");
				return;
			}
			shader.GetKernelThreadGroupSizes(index, out threadX, out threadY, out threadZ);
		}
	}

	/**
	 * A sample GPU buffer associated to a shader compute buffer
	 */
	private class VoxelBuffer : System.IDisposable
	{

		/**
		 * The shader compute buffer in which the samples are being used
		 */
		public ComputeBuffer CBuffer { get { return buffer; } }

		/**
		 * The size of the voxel
		 */
		public float VoxelSize { get { return size; } }

		private float size;
		private ComputeBuffer buffer;

		public VoxelBuffer (ComputeBuffer buf, float s)
		{
			buffer = buf;
			size = s;
		}

		/**
		 * Returns the samples of the GPU buffer
		 */
		public Voxel[] GetData() {
			var data = new Voxel[CBuffer.count];
			CBuffer.GetData (data);
			return data;
		}

		/**
		 * Release the GPU Buffer
		 */
		public void Dispose() {
			buffer.Release ();
		}
	}

    /**
	 * Wrapper for computing the sampling on GPU
	 */
    private class GPUSampler
    {
        public static VoxelBuffer Sample(
            ComputeShader shader,
            Mesh source,
            int countByAxis,
            float phy, float labmda,
            int samplingMaximumIterations) 
        {
            // Pre process
            source.RecalculateBounds();

            // Size utils
            Bounds bounds = source.bounds;
            float BBSize = Mathf.Max(bounds.size.x,
                Mathf.Max(bounds.size.y, bounds.size.z));
            float VSize = BBSize / countByAxis;
            float VThreshold = phy * VSize;
            Vector3 realSize = bounds.size;
            int grid_width = Mathf.CeilToInt(realSize.x / VSize) + 1;
            int grid_height = Mathf.CeilToInt(realSize.y / VSize) + 1;
            int grid_depth = Mathf.CeilToInt(realSize.z / VSize) + 1;

            Debug.Log("Voxel Size: " + VSize);
            Debug.Log("Voxel Radius: " + VSize * 0.5f);
            Debug.Log("Voxel Grid width x height x depth: " + 
                grid_width + " x " + grid_height + " x " + grid_depth);
            Debug.Log("Sampling triangles edges threshold: " + VThreshold);
            Debug.Log("Sampling max iterations: " + samplingMaximumIterations);

            // Buffer setup
            ComputeBuffer verticesBuffer = new ComputeBuffer(source.vertices.Length,
                Marshal.SizeOf(typeof(Vector3)));
            verticesBuffer.SetData(source.vertices);

            ComputeBuffer trisBuffer = new ComputeBuffer(source.triangles.Length,
                Marshal.SizeOf(typeof(int)));
            trisBuffer.SetData(source.triangles);

            ComputeBuffer outputBuffer = new ComputeBuffer(
                grid_width * grid_height * grid_depth,
                Marshal.SizeOf(typeof(Voxel))
            );

            // Compute function link
            Kernel kernel = new Kernel(shader, "ComputeSampling");

            // Map bounds on GPU
            shader.SetVector("_Start", bounds.min);
            shader.SetVector("_End", bounds.max);
            shader.SetVector("_Size", bounds.size);


            shader.SetFloat("_Unit", VSize);
            shader.SetFloat("_lambda", labmda);
            shader.SetFloat("_HalfUnit", VSize * 0.5f);
            shader.SetInt("_Width", grid_width);
            shader.SetInt("_Height", grid_height);
            shader.SetInt("_Depth", grid_depth);
            shader.SetInt("_SamplingMaxIterations", samplingMaximumIterations);
            shader.SetFloat("_SamplingDistanceThreshold", VThreshold);

            // Map mesh on GPU
            shader.SetBuffer(kernel.Index, "_VertBuffer", verticesBuffer);
            shader.SetInt("_TrianglesCount", trisBuffer.count);
            shader.SetBuffer(kernel.Index, "_TriBuffer", trisBuffer);
            shader.SetBuffer(kernel.Index, "_VoxelBuffer", outputBuffer);

            int max_grid_size = Math.Max(grid_depth, Math.Max(grid_width, grid_height));
            // GO
            shader.Dispatch(kernel.Index,
                max_grid_size / (int)kernel.ThreadX + 1,
                max_grid_size / (int)kernel.ThreadY + 1,
                max_grid_size / (int)kernel.ThreadZ + 1);

            // Free
            verticesBuffer.Dispose();
            trisBuffer.Dispose();

            return new VoxelBuffer(outputBuffer, VSize);
        }
    }

    // WEIGTHING RELATED

    private struct WeightingTexturePixel
    {
        /**
         * Id of the triangle
         * 0: No triangle on this pixel
         * id - 1: Triangle mesh real id
         */
        public int id; 
    }

    // PUBLIC MEMBERS SAMPLING
    public ComputeShader sampling_compute_shader;
    public int sampling_resolution = 32;
    public bool sampling_show_samples = true;
    public bool remove_close_samples = true;
    public float sample_debug_size = 0.1f;
    // When generating a sample, if the distance to the center of the center
    // is higher than 1/sampling_threshold_on_voxel_size * voxel_radius, we don't use it
    public int sampling_neighbour_distance_threshold = 50;
    // Maximum amount of tests before switching to the next triangle
    public int sampling_maximum_iterations = 100;
    public int sampling_debug_threshold = 10;
    public float sampling_lambda = 5;
    public float sampling_phy = 0.1f;

    // PUBLIC MEMBERS WEIGHTING
    public bool weighting_render_continious = false;
    public bool weighting_render = true;
    public Material weighting_material;
    public ComputeShader weighting_compute_shader;
    public int width_resolution = 0;

	// PRIVATE MEMBERS SAMPLING
    private Mesh m_mesh;
    private VoxelBuffer m_voxel_buffer;
    private List<Sample> m_sample_data;
    private float m_voxel_size;

	// PRIVATE MEMBERS WEIGHTING
    private int m_renderer_width;
    private int m_renderer_height;
    private RenderTextureFormat m_weighting_texture_format;
    private int m_weighting_texture_depth;
    private ComputeBuffer m_weighting_cbuffer;
    private RenderTexture m_weighting_render_tex;
    private Matrix4x4 m_matrix_view;
    private Matrix4x4 m_matrix_project;
    private Matrix4x4 m_matrix_project_cpu;
    private Matrix4x4 m_matrix_model;
    private Kernel m_compute_kernel;
    // Keep track of samples per pixel
    private List<List<Sample>> m_triangles_samples;
    private int m_triangle_count;

    // PUBLIC MEMBERS TRANSFORM
    public Material m_rendering_shader;
    public bool compute_transforms = true;

    // PRIVATE MEMBERS TRANSFORM
    private Vector2 m_samples_centroid; // Weigthed centroid in image space of the last frame
    private Vector2 m_image_space_o;
    private Vector2 m_image_space_u;
    private Vector2 m_image_space_v;

    // OTHER
    private TimeLogger m_time_logger;


    void Start()
    {
        Application.targetFrameRate = 25;
        // General preparation
        StartGeneric();

        // Voxel preparation
        StartVoxel();

        // Sample weighting perpartation
        StartWeighting();
    }

    void StartGeneric()
    {
        m_mesh = GetComponent<MeshFilter>().mesh;
        m_time_logger = new TimeLogger();
    }

    void StartVoxel()
    {

        m_time_logger.start();
        m_voxel_buffer = GPUSampler.Sample(sampling_compute_shader, 
            m_mesh, sampling_resolution,
            sampling_phy, sampling_lambda,
            sampling_maximum_iterations);
        m_time_logger.log("[T01] GPU sample generation: ");

        m_voxel_size = m_voxel_buffer.VoxelSize;

        m_time_logger.start();
        var voxel_buffer_data = m_voxel_buffer.GetData();
        m_voxel_buffer.Dispose();
        m_time_logger.log("[T02] GPU -> CPU sample copy: ");
        // Create a list of samples based on voxels
        m_sample_data = new List<Sample>();

        m_time_logger.start();
        for (int v = 0, vn = voxel_buffer_data.Length; v < vn; ++v)
        {
            Voxel voxel = voxel_buffer_data[v];
            if (voxel.flag)
            {
                Sample sample = new Sample();
                sample.triangle_id = (uint) voxel.triangle_id;
                //sample.position = GetSampleWorldPosition((int) (sample.triangle_id * 3), voxel.t1, voxel.t2);
                sample.position = voxel.position;
                sample.ci = new Vector2();
                sample.pi = new Vector2();
                m_sample_data.Add(sample);
            }
        }
        m_time_logger.log("[T03] Create sample objects: ");

        Debug.Log(m_sample_data.Count + " samples generated");
        if (remove_close_samples)
            RemoveCloseSamples();
        Debug.Log(m_sample_data.Count + " left after reduction");
    }

    void RemoveCloseSamples()
    {
        // Create octree
        Bounds bounds = m_mesh.bounds;
        float max_axis_size = Mathf.Max(bounds.size.x,
            Mathf.Max(bounds.size.y, bounds.size.z));
        float min_node_size = m_voxel_size * 2.0f;
        Debug.Log("Octree minimum node size: " + min_node_size);
        PointOctree<SampleNode> samplesTree = new PointOctree<SampleNode>(
            max_axis_size, bounds.center, min_node_size);


        // Add samples in tree
        m_time_logger.start();
        for (int s = 0, ns = m_sample_data.Count; s < ns; ++s)
        {
            Sample sample = m_sample_data[s];
            SampleNode node = new SampleNode();
            node.sample = sample;
            node.neighbours = new List<SampleNode>();
            samplesTree.Add(node, node.sample.position);
        }
        m_time_logger.log("[T10] Create octree: ");

        // Calculate neighbours sets
        float threshold_distance = m_voxel_size * 0.5f;
        Debug.Log("Sample reduction distance threshold: " + threshold_distance);

        List<SampleNode> nonEmptySamples = new List<SampleNode>();
        List<SampleNode> allSamples = samplesTree.GetAll();

        m_time_logger.start();
        for (int j = 0, jn = allSamples.Count; j < jn; ++j)
        { 
            SampleNode node = allSamples[j];

            // Create neighbours sets
            node.centroid = new Vector3();

            List<SampleNode> closeSamples = samplesTree.GetNearby(
                node.sample.position, threshold_distance
            );

            for (int i = 0, n = closeSamples.Count; i < n; ++i)
            {
                SampleNode neighbour = closeSamples[i];
                if (neighbour != node)
                {
                    node.neighbours.Add(neighbour);
                    node.centroid += neighbour.sample.position;
                }
            }

            // Process the centroid, if needed
            if (node.neighbours.Count > 0)
            {
                nonEmptySamples.Add(node);
                node.centroid /= node.neighbours.Count;
            }
        }
        m_time_logger.log("[T11] Create samples neighbours sets: ");

        // Remove neighbours
        SampleNode candidate = null;
        float maxdist = 0.0f, dist = 0.0f;

        m_time_logger.start();
        while (nonEmptySamples.Count > 0)
        {
            candidate = null;
            maxdist = -1.0f;

            // Find the one the most far away from its neighbours
            for(int i = 0, n = nonEmptySamples.Count; i < n; ++i)
            {
                SampleNode node = nonEmptySamples[i];
                dist = (node.sample.position - node.centroid).sqrMagnitude;
                if (dist > maxdist)
                {
                    maxdist = dist;
                    candidate = node;
                }
            }

            // Remove it.
            List<SampleNode> node_to_updates = candidate.neighbours;
            m_sample_data.Remove(candidate.sample);
            nonEmptySamples.Remove(candidate);
            samplesTree.Remove(candidate);

            // Updates neighbours
            for (int i = 0, n = node_to_updates.Count; i < n; ++i)
            {
                SampleNode node = node_to_updates[i];
                if (node.neighbours.Remove(candidate))
                {
                    if (node.neighbours.Count > 0)
                    {
                        // Recalculate its centroid
                        node.centroid -= (candidate.sample.position / ((float) node.neighbours.Count + 1.0f));
                    }
                    else
                    {
                        // It doesn't need to be removed anymore
                        nonEmptySamples.Remove(node);
                    }
                }
            }
        }
        m_time_logger.log("[T12] Remove redundant samples: ");
    }

    void StartWeighting()
    {
        if (!SystemInfo.supportsComputeShaders)
            Debug.LogError("Compute shaders aren't supported");

        m_rendering_shader = GetComponent<Renderer>().material;

        if (m_rendering_shader == null)
            Debug.LogError("Material not set");

        Display main_display = Display.main;

        if (width_resolution == 0) width_resolution = Screen.width;
        m_renderer_width = width_resolution;
        m_renderer_height = (int)(1.0f/Camera.main.aspect * m_renderer_width);
        Debug.Log("Texture size: " + m_renderer_width  + " x " + m_renderer_height);
        m_weighting_texture_format = RenderTextureFormat.RInt;

        if (!SystemInfo.SupportsRenderTextureFormat(m_weighting_texture_format))
            Debug.LogError("Weighting texture format not supported");

        // Attach itself to the camera
        Camera.main.GetComponent<CameraPreJobs>().AddPreJob(this);

        m_weighting_texture_depth = 32;
        m_weighting_render_tex = new RenderTexture(
            m_renderer_width, m_renderer_height, m_weighting_texture_depth,
            m_weighting_texture_format, RenderTextureReadWrite.Linear);
        m_weighting_render_tex.name = "_Texture";

        // Create list of triangles samples
        m_triangle_count = m_mesh.triangles.Length / 3;
        Debug.Log("Triangles count: " + m_triangle_count);
        /* 
        m_triangles_samples = new List<List<Sample>>(m_triangle_count);

        for (int i = 0, n = m_triangle_count; i < n; ++i)
        {
            m_triangles_samples.Add(new List<Sample>());
        }

        for (int i = 0, n = m_sample_data.Count; i < n; ++i)
        {
            Sample sample = m_sample_data[i];
            m_triangles_samples[sample.triangle_id].Add(sample);
        }
        */

        m_samples_centroid = new Vector2();
        m_image_space_o = new Vector2(0, 0);
        m_image_space_u = new Vector2(1, 0);
        m_image_space_v = new Vector2(0, 1);
    }

    public void PreFrameJob()
    {
        m_matrix_project = GL.GetGPUProjectionMatrix(Camera.main.projectionMatrix, true);
        m_matrix_project_cpu = GL.GetGPUProjectionMatrix(Camera.main.projectionMatrix, false);
        m_matrix_model = transform.localToWorldMatrix;
        m_matrix_view = Camera.main.worldToCameraMatrix;
        if(weighting_render && m_mesh != null)
        {
            if (!weighting_render_continious) weighting_render = false;
            DoWeighting();
            if (compute_transforms)
                ComputeTransforms();
        }
    }

    void DoWeighting()
    {

        m_time_logger.start();
        // Clear texture
        {
            RenderTexture current = UnityEngine.RenderTexture.active;
            UnityEngine.RenderTexture.active = m_weighting_render_tex;
            GL.Clear(true, true, Color.clear);
            // Reset active target
            UnityEngine.RenderTexture.active = current;
        }

        Graphics.SetRenderTarget(m_weighting_render_tex);

        if (!weighting_material.SetPass(0))
            Debug.LogError("Unable to set weighting material pass to 0");

        // Prepare
        weighting_material.SetMatrix("_MATRIX_M", m_matrix_model);
        weighting_material.SetMatrix("_MATRIX_V", m_matrix_view);
        weighting_material.SetMatrix("_MATRIX_P", m_matrix_project);

        // Render
        Graphics.DrawMeshNow(m_mesh, transform.position, transform.rotation);
        //m_time_logger.log("[T20] Drawing visible triangles: ");

        // Compute pixels of the texture on GPU
        ComputeBuffer output_buffer = new ComputeBuffer(
            m_renderer_height * m_renderer_width,
            Marshal.SizeOf(typeof(WeightingTexturePixel))
        );

        // Clear data
        m_time_logger.start();
        WeightingTexturePixel[] clearing_data = new WeightingTexturePixel[output_buffer.count];
        WeightingTexturePixel clearing_value = new WeightingTexturePixel();
        clearing_value.id = 0;
        for (int i = 0, n = output_buffer.count; i < n; ++i)
        {
            clearing_data[i] = clearing_value;
        }
        output_buffer.SetData(clearing_data);
        //m_time_logger.log("[T21] Clear GPU visible triangles buffer: ");

        if (!weighting_compute_shader.HasKernel("ReadTextureIDS"))
            Debug.LogError("Weighting compute shader kernel not found");

        m_compute_kernel = new Kernel(weighting_compute_shader, "ReadTextureIDS");
        weighting_compute_shader.SetInt("_Width", m_renderer_width);
        weighting_compute_shader.SetInt("_Height", m_renderer_height);
        weighting_compute_shader.SetTexture(m_compute_kernel.Index, "_Texture", m_weighting_render_tex);
        weighting_compute_shader.SetBuffer(m_compute_kernel.Index, "_IDS", output_buffer);

        // Compute
        m_time_logger.start();
        weighting_compute_shader.Dispatch(m_compute_kernel.Index,
            m_renderer_width / (int) m_compute_kernel.ThreadX + 1,
            m_renderer_height / (int) m_compute_kernel.ThreadY + 1,
            (int) m_compute_kernel.ThreadZ);
        //m_time_logger.log("[T22] Computing triangle ids on GPU: ");

        m_time_logger.start();
        var shader_data = new WeightingTexturePixel[output_buffer.count];
        output_buffer.GetData(shader_data);
        output_buffer.Release();
        //m_time_logger.log("[T23] Copying data to CPU: ");

        // New weights
        Debug.Assert(shader_data.Length == m_renderer_width * m_renderer_height);
        Matrix4x4 toImageMat = m_matrix_project_cpu * m_matrix_view * m_matrix_model;
        Vector3 cimage_location = new Vector3();
        Vector2Int iimage_location = new Vector2Int();
        //Vector4 wtest;
        int pixel_id = 0;
        int pixel_triangle_id = 0;
        bool visible;

        // Pour chaque sample
        m_time_logger.start();
        for (int i = 0, n = m_sample_data.Count; i < n; ++i)
        {
            // Reset weights
            Sample s = m_sample_data[i];
            s.pweight = s.cweight;
            s.pi = s.ci;

            // Compute current weight

            // Clip space
            cimage_location = toImageMat.MultiplyPoint(s.position);
            //wtest = toImageMat * new Vector4(s.position.x, s.position.y, s.position.z, 1);
            //wtest *= wtest.w;
            //cimage_location.x = wtest.x;
            //cimage_location.y = wtest.y;
            //Debug.Log(wtest.x + " compared to " + cimage_location.x);
            /*
            // Screen space
            image_location.x /= image_location.w;
            image_location.y /= image_location.w;
            */
            // Image space [-1, 1] to [0, 1]
            cimage_location.x = (cimage_location.x + 1.0f) * 0.5f;
            cimage_location.y = (cimage_location.y + 1.0f) * 0.5f;
            // Pixel space
            iimage_location.x = (int) Math.Round(s.pimage_location.x * (float) m_renderer_width);
            iimage_location.y = (int) Math.Round(s.pimage_location.y * (float) m_renderer_height);
            s.pimage_location = cimage_location;
            //Debug.Assert(iimage_location.x == image_location.x * m_renderer_width);
            //Debug.Assert(iimage_location.y == image_location.y * m_renderer_height);

            // In image ? 
            pixel_id = iimage_location.x + iimage_location.y * m_renderer_width;
            visible = pixel_id < shader_data.Length && pixel_id > 0;

            if (visible)
            {
                pixel_triangle_id = shader_data[pixel_id].id;
                // Correct triangle on this pixel ?
                visible = pixel_triangle_id != 0 && pixel_triangle_id - 1 == s.triangle_id;
            }

            if (visible)
                s.cweight = 1.0f;
            else
                s.cweight = 0.0f;
        }
        //m_time_logger.log("[T24] Compute samples visibility: ");
    }

    void ComputeTransforms()
    {
        m_time_logger.start();
        Vector2 last_centroid = m_samples_centroid;
        Vector2 cur_centroid = new Vector2();

        int count = 0;

        for (int i = 0, n = m_sample_data.Count; i < n; i += 1)
        {
            Sample sample = m_sample_data[i];
            if (sample.cweight > 0.0f && sample.pweight > 0.0f)
            {
                sample.ci = sample.pimage_location;
                cur_centroid += sample.ci;
                count++;
            }
        }

        if (count == 0) return;

        cur_centroid /= count;

        Debug.Log("Centroid: " + cur_centroid);

        m_samples_centroid = cur_centroid;

        // Compute ci and pi of each sample
        // And compute values for roration estimation

        float zx = 0.0f;
        float zy = 0.0f;
        float zd = 0.0f;
        float zci = 0.0f;
        bool first = true;

        for (int i = 0, n = m_sample_data.Count; i < n; i += 1)
        {
            Sample sample = m_sample_data[i];
            if (sample.cweight > 0.0f && sample.pweight > 0.0f)
            {
                sample.ci -= cur_centroid;
                sample.pi -= last_centroid;
                if (first) // First sample
                {
                    zx = sample.cweight * (Vector2.Dot(sample.pi, sample.ci));
                    zy = sample.cweight * (sample.pi.x * sample.ci.y - sample.pi.y * sample.ci.x);
                    zd = sample.cweight * Vector2.SqrMagnitude(sample.pi);
                    zci = sample.cweight * Vector2.SqrMagnitude(sample.ci);
                    first = false;
                }
                else
                {
                    zx += sample.cweight * (Vector2.Dot(sample.pi, sample.ci));
                    zy += sample.cweight * (sample.pi.x * sample.ci.y - sample.pi.y * sample.ci.x);
                    zd += sample.cweight * Vector2.SqrMagnitude(sample.pi);
                    zci += sample.cweight * Vector2.SqrMagnitude(sample.ci);
                }
            }
        }

        Vector2 tr_z = new Vector2(zx, zy) / zd;

        // Symetric Horn solution
        float s = Mathf.Pow(tr_z.magnitude, -1) * Mathf.Sqrt(zci / zd);
        tr_z *= s;

        Debug.Log("Z Transform: " + tr_z);

        // Texture coordinates
        m_image_space_o = new Vector2(0, 0);
        m_image_space_u = new Vector2(1, 0);
        m_image_space_v = new Vector2(0, 1);

        m_image_space_o = cur_centroid + ComplexTransform(tr_z, m_image_space_o - last_centroid);
        m_image_space_u = ComplexTransform(tr_z, m_image_space_u);
        m_image_space_v = ComplexTransform(tr_z, m_image_space_v);

        Debug.Log("U: " + m_image_space_u + " V: " + m_image_space_v + "  O: " + m_image_space_o);
        m_rendering_shader.SetFloat("_Pattern_Origin_X", m_image_space_o.x);
        m_rendering_shader.SetFloat("_Pattern_Origin_Y", m_image_space_o.y);
        m_rendering_shader.SetFloat("_Pattern_U_X", m_image_space_u.x);
        m_rendering_shader.SetFloat("_Pattern_U_Y", m_image_space_u.y);
        m_rendering_shader.SetFloat("_Pattern_V_X", m_image_space_v.x);
        m_rendering_shader.SetFloat("_Pattern_V_Y", m_image_space_v.y);
        m_rendering_shader.SetFloat("_Pattern_U_SqrMag", m_image_space_u.SqrMagnitude());
        m_rendering_shader.SetFloat("_Pattern_V_SqrMag", m_image_space_v.SqrMagnitude());
        //m_time_logger.log("[T25] Compute pattern transformation: ");
    }

    Vector2 ComplexTransform(Vector2 z, Vector2 a)
    {
        Vector2 tr_scale = new Vector2(z.magnitude, z.magnitude);
        float tr_rotate = Vector2.Angle(new Vector2(1, 0), z);
        float tr_rotate_sin = Mathf.Sin(tr_rotate);
        float tr_rotate_cos = Mathf.Cos(tr_rotate);
        // Scale 
        Vector2 sc_a = Vector2.Scale(a, tr_scale);
        // Rotate
        return new Vector2(
            sc_a.x * tr_rotate_cos - sc_a.y * tr_rotate_sin,
            sc_a.x * tr_rotate_sin + sc_a.y * tr_rotate_cos
        );
    }

    void Update()
    {
    }

    private Vector3 GetSampleWorldPosition(int triangle, float t1, float t2)
    {
        int ta = m_mesh.triangles[triangle];
        int tb = m_mesh.triangles[triangle + 1];
        int tc = m_mesh.triangles[triangle + 2];

        Vector3 va = m_mesh.vertices[ta];
        Vector3 vb = m_mesh.vertices[tb];
        Vector3 vc = m_mesh.vertices[tc];

        return va + t1 * (vb - va) + t2 * (vc - va);
    }

    void OnDrawGizmos()
    {
        if (sampling_show_samples && m_sample_data != null)
        {
            Gizmos.color = Color.blue;
            float t = (float) sampling_debug_threshold / 100.0f;
            int step = (int) Mathf.Lerp(10.0f, 1.0f, t);

            for (int i = 0, n = m_sample_data.Count; i < n; i += step)
            {
                Sample sample = m_sample_data[i];
                if (sample.cweight > 0.0f)
                    Gizmos.color = Color.green;
                else
                    Gizmos.color = Color.red;
                //Gizmos.color = sample.debugColor;
                Gizmos.DrawSphere(
                    m_matrix_model.MultiplyPoint(sample.position),
                    sample_debug_size);
            }
        }
    }
}
