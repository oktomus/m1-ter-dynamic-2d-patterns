#if !defined(D2P_PATTERN_VERT_INCLUDED)
#define D2P_PATTERN_VERT_INCLUDED

#include "D2P_Pattern_Types.cginc"

Interpolators vert (VertexData v)
{
	Interpolators o;
	o.position = UnityObjectToClipPos(v.position);
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.normal = UnityObjectToWorldNormal(v.normal);
	o.world_pos = mul(unity_ObjectToWorld, v.position);
	return o;
}

#endif