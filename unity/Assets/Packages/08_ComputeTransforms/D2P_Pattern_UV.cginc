#if !defined(D2P_PATTERN_UV_INCLUDED)
#define D2P_PATTERN_UV_INCLUDED

#include "D2P_Pattern_Types.cginc"

float2 compute_uv(Interpolators i, float4 screen_space)
{
	if (_UseModelUVs > 0)
		return i.uv;

	float su = screen_space.x / _ScreenParams.x;
	float sv = screen_space.y / _ScreenParams.y;
	float2 q = float2(su, sv);
	float2 pattern_o = float2(_Pattern_Origin_X, _Pattern_Origin_Y);
    float2 pattern_u = float2(_Pattern_U_X, _Pattern_U_Y);
	float2 pattern_v = float2(_Pattern_V_X, _Pattern_V_Y);
	float2 qo = q - pattern_o;
	float2 uv = float2(
		dot(qo, pattern_u) / _Pattern_U_SqrMag,
		dot(qo, pattern_v) / _Pattern_V_SqrMag
	);
    return uv;
}

#endif