﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "D2P/AlphaPattern"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Tint ("Tint", Color) = (1,1,1,1)
		_Paint ("Paint", Color) = (1,0,0,1)
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5
		[Gamma] _Metallic ("Metallic", Range(0, 1)) = 0
		[Toggle] _UseModelUVs("Use Model UVS", Float) = 0
	}
	SubShader
	{
		// Main light
		Pass
		{
			Tags { 
				"LightMode" = "ForwardBase"
				"RenderType"="Opaque" 
			}


			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag

			#include "D2P_PatternShader.cginc"
			
			ENDCG
		}

		// Second lights
		Pass
		{
			Tags { 
				"LightMode" = "ForwardAdd"
			}
			ZWrite Off
			Blend One One

			CGPROGRAM
			#pragma target 3.0

			#pragma multi_compile DIRECTIONAL POINT SPOT

			#pragma vertex vert
			#pragma fragment frag

			#include "D2P_PatternShader.cginc"
			
			ENDCG
		}

		// Fill
		Pass
		{
			ZWrite Off
			Blend OneMinusDstAlpha One, One Zero


			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "D2P_Pattern_Types.cginc"
			#include "D2P_Pattern_Vert.cginc"

			fixed4 frag (Interpolators i, float4 screenSpace : SV_Position) : SV_Target
			{
				return _Paint;
			}
			
			ENDCG
		}
	}
}
