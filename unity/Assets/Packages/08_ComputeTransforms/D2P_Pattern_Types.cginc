#if !defined(D2P_PATTERN_TYPES_INCLUDED)
#define D2P_PATTERN_TYPES_INCLUDED

sampler2D _MainTex;
float4 _Tint;
float4 _Paint;
float _Smoothness;
float _Metallic;
float _UseModelUVs;

float4 _MainTex_ST;
float _Pattern_Origin_X;
float _Pattern_Origin_Y;
float _Pattern_U_X;
float _Pattern_U_Y;
float _Pattern_V_X;
float _Pattern_V_Y;
float _Pattern_U_SqrMag;
float _Pattern_V_SqrMag;

struct VertexData
{
	float4 position: POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
};

struct Interpolators
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 world_pos : TEXCOORD2;
};

#endif