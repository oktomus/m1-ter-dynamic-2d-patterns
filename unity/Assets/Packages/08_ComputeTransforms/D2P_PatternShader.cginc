﻿#if !defined(D2P_PATTERN_INCLUDED)
#define D2P_PATTERN_INCLUDED

#include "AutoLight.cginc"
#include "UnityPBSLighting.cginc"
#include "D2P_Pattern_UV.cginc"
#include "D2P_Pattern_Types.cginc"
#include "D2P_Pattern_Vert.cginc"

UnityLight CreateLight (Interpolators i, float3 color) {
	UnityLight light;

#if defined(POINT) || defined(SPOT)
	light.dir = normalize(_WorldSpaceLightPos0.xyz - i.world_pos);
#else
	light.dir = _WorldSpaceLightPos0.xyz;
#endif

	UNITY_LIGHT_ATTENUATION(attenuation, 0, i.world_pos);
	light.color = color.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

fixed4 frag (Interpolators i, float4 screenSpace : SV_Position) : SV_Target
{
	i.normal = normalize(i.normal);

	float3 view_dir = normalize(_WorldSpaceCameraPos - i.normal);

	// Uv coordinates
	float2 tuv = compute_uv(i, screenSpace);

	// Albedo
	float3 albedo = float3(1, 1, 1);
	//float3 albedo = tex2D(_MainTex, tuv);

	// Specular
	float3 specular_tint;
	float one_minus_reflexivity;
	albedo = DiffuseAndSpecularFromMetallic(
		albedo, _Metallic, specular_tint, one_minus_reflexivity
	);

	UnityIndirect indirect_light;
	indirect_light.diffuse = 0;
	indirect_light.specular = 0;

	// Compute
	float4 result = UNITY_BRDF_PBS(
		albedo,
		specular_tint,
		one_minus_reflexivity,
		_Smoothness,
		i.normal,
		view_dir,
		CreateLight(i, _LightColor0.rgb),
		indirect_light
	);

	float4 alpha_result = UNITY_BRDF_PBS(
		albedo,
		specular_tint,
		one_minus_reflexivity,
		_Smoothness,
		i.normal,
		view_dir,
		CreateLight(i, float3(1, 1, 1)),
		indirect_light
	);

	//return alpha_result;
	float alpha = tex2D(_MainTex, tuv).a;
	alpha *= max(alpha_result.r, max(alpha_result.g, alpha_result.b));
	result.a = alpha;
	//return result;
	//return fixed4(alpha, 0, 0, 1);
	return lerp(fixed4(0, 0, 0, 0), result, alpha);

	// sample the texture
	//tex_color.rgb = i.diff.rgb;
	//tex_color.a *= i.diff.a;

	//return fixed4(i.diff.a * tex_color.a, 0, 0, 1);
	//return lerp(_Paint, tex_color, tex_color.a);
}

#endif