﻿using System;
using UnityEngine;

namespace Voxelizer
{
	public class Voxel
	{
		public Vector3 position;
		public bool activated;

		public Voxel (Vector3 position, bool state)
		{
			this.position = position;
			this.activated = state;
		}
	}
}

