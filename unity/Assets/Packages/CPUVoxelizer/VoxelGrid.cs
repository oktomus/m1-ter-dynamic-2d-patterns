﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 *  http://blog.wolfire.com/2009/11/Triangle-mesh-voxelization
 *  https://nihilistdev.blogspot.fr/2012/08/voxelization-in-unity.html
 *  https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
 *  https://github.com/mattatz/unity-voxel/blob/master/Assets/Packages/Voxelizer/Scripts/Voxelizer.cs
 */


namespace Voxelizer
{
	public class VoxelGrid
	{
		private Vector3 center;
		private float width, height, depth;	
		private float voxelSize;


		private class Triangle
		{
			public Vector3 a, b, c;

			public Triangle(Vector3 a, Vector3 b, Vector3 c) {
				this.a = a;
				this.b = b;
				this.c = c;
			}

			public bool Hit (Ray ray, out float distance) {
				distance = -1f;

				var e1 = this.b - this.a;
				var e2 = this.c - this.a;
				var P = Vector3.Cross(ray.direction, e2);
				var det = Vector3.Dot(e1, P);

				if (det > -float.Epsilon && det < float.Epsilon) return false;

				float invDet = 1f / det;

				var T = ray.origin - this.a;
				var u = Vector3.Dot(T, P) * invDet;
				if (u < 0f || u > 1f) return false;

				var Q = Vector3.Cross(T, e1);
				var v = Vector3.Dot(ray.direction, Q * invDet);
				if (v < 0f || u + v > 1f) return false;

				var t = Vector3.Dot(e2, Q) * invDet;
				if(t > float.Epsilon) {
					distance = t;
					return true;
				}

				return false;
			}
		}

		private class TriangleHit
		{
			public Triangle triangle;
			public float distance;

			public TriangleHit(Triangle triangle, float distance) {
				this.triangle = triangle;
				this.distance = distance;
			}
		}
		
		public VoxelGrid (Vector3 center, float width, float height, float depth, float voxelSize)
		{
			this.center = center;
			this.width = width;
			this.height = height;
			this.depth = depth;
			this.voxelSize = voxelSize;
		}

		public VoxelGrid (Vector3 center, Vector3 size, float voxelSize) : this(center, size.x, size.y, size.z, voxelSize)
		{
		}

		public List<Voxel> GenerateVoxels(Mesh mesh)
		{
			List<Voxel> reslist = new List<Voxel>();

			int countbyx = (int)(Math.Ceiling (width / voxelSize));
			int countbyy = (int)(Math.Ceiling (height / voxelSize));
			int countbyz = (int)(Math.Ceiling (depth / voxelSize));

			Debug.Log ("Voxel total count: " + countbyx * countbyy * countbyz);
			Debug.Log ("X: " + countbyx + ", Y: " + countbyy + ", Z: " + countbyz);

			var halfsize = voxelSize * 0.5f;

			var triangles = new List<Triangle>();
			for(int i = 0, n = mesh.triangles.Length; i < n; i += 3) {
				triangles.Add(
					new Triangle(
						mesh.vertices[mesh.triangles[i]],
						mesh.vertices[mesh.triangles[i + 1]],
						mesh.vertices[mesh.triangles[i + 2]]
					)
				);
			}

			for (int vx = 0; vx < countbyx; vx++) {
				for (int vy = 0; vy < countbyy; vy++) {
					var ray = new Ray (
						          new Vector3 (
							          (center.x - width / 2.0f) + vx * voxelSize + halfsize,
							          (center.y - height / 2.0f) + vy * voxelSize + halfsize,
								      (center.z - depth / 2.0f) - halfsize),
						          Vector3.forward);
					List<TriangleHit> hits;
					if (RayHitsTriangles(ray, triangles, out hits)) {
						// Extend list
						reslist.AddRange(GenerateVoxelsFromHits (ray, hits, voxelSize));
					}
				}
			}
				

			return reslist;
		}

		static float ReelToGrid(float dist, float size)
		{
			return Mathf.FloorToInt (dist / size) * size;
		}

		static List<Voxel> GenerateVoxelsFromHits(Ray ray, List<TriangleHit> hits, float size) {
			List<Voxel> reslist = new List<Voxel>();



			// Like scan line algorithm
			// --------x---------x---------x--------x---------
			// EMPTY     INSIDE     EMPTY    INSIDE
			//         xxxxxxxxxxx         xxxxxxxxxx
			for (int i = 0; i < hits.Count; i++) {
				if (i % 2 == 0 && i == hits.Count - 1) {
					reslist.Add (new Voxel (ray.origin +
					ReelToGrid (hits [i].distance, size) * ray.direction, true));
				} else if (i % 2 != 0) {
					var from = ReelToGrid (hits [i - 1].distance, size);
					var to = ReelToGrid (hits [i].distance, size);
					for(float distance = from; distance < to; distance += size) {
						reslist.Add (new Voxel (ray.origin + distance * ray.direction, true));
					}
				} else {
					continue;
				}
			}

			return reslist;
		}

		static bool RayHitsTriangles(Ray ray, List<Triangle> triangles, out List<TriangleHit> hits)
		{
			hits = new List<TriangleHit> ();
			foreach(Triangle tr in triangles) {
				float dist;
				if (tr.Hit (ray, out dist)) {
					hits.Add(new TriangleHit (tr, dist));
				}	
			}
			hits = hits.OrderBy (r => r.distance).ToList ();
			return hits.Count > 0;
		}

		public void DrawBorders()
		{			
			Vector3 a = new Vector3();
			Vector3 b = new Vector3();

			int countbyx = (int)(Math.Ceiling (width / voxelSize));
			int countbyy = (int)(Math.Ceiling (height / voxelSize));
			int countbyz = (int)(Math.Ceiling (depth / voxelSize));

			// width lines
			a.x = center.x - width / 2.0f;
			b.x = a.x + voxelSize * countbyx;
			for(int vz = 0; vz <= countbyz; vz++) {
				for(int vy = 0; vy <= countbyy; vy++)
				{
					a.z = b.z = (center.z - depth / 2.0f) + vz * voxelSize;
					a.y = b.y = (center.y - height / 2.0f) + vy * voxelSize;			
					Debug.DrawLine (a, b);
				}			
			}


			// depth lines
			a.z = center.z - depth / 2.0f;
			b.z = a.z + voxelSize * countbyz;
			for(int vx = 0; vx <= countbyx; vx++) {
				for(int vy = 0; vy <= countbyy; vy++)
				{
					a.x = b.x = (center.x - width / 2.0f) + vx * voxelSize;
					a.y = b.y = (center.y - height / 2.0f) + vy * voxelSize;			
					Debug.DrawLine (a, b);
				}			
			}



			// height lines
			a.y = center.y - height / 2.0f;
			b.y = a.y + voxelSize * countbyy;
			for(int vx = 0; vx <= countbyx; vx++) {
				for(int vz = 0; vz <= countbyz; vz++) {
					a.x = b.x = (center.x - width / 2.0f) + vx * voxelSize;
					a.z = b.z = (center.z - depth / 2.0f) + vz * voxelSize;		
					Debug.DrawLine (a, b);
				}			
			}


						
		}
	}
}

