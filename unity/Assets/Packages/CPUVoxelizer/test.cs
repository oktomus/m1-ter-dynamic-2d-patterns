﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Voxelizer;

/**
 * Test execution time and voxelization results on CPU
 * 
 * Results:
 *  For a mesh of 32530 verts, generating 102764 voxels takes 433 seconds, or ~7minutes.
 *  With that amount of voxel, it is hard to display them in the viewport by instancing objects
 */

[RequireComponent(typeof(MeshFilter))]
public class test : MonoBehaviour {

	private VoxelGrid vg;
	private List<Voxel> voxels;

	public float size = 0.2f;
	public bool refresh = true;
	public bool drawGrid = true;
	public bool drawVoxels = true;

	public GameObject voxelInstance;
	private List<GameObject> voxelsInstance;
	// Use this for initialization
	void Start () {
		voxelsInstance = new List<GameObject> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(refresh) {
			float then = Time.realtimeSinceStartup;

			Mesh mesh = GetComponent<MeshFilter> ().mesh;

			Bounds bounds = mesh.bounds;
			Vector3 center = bounds.center;
			Debug.Log ("Size is " + bounds.size);
			Debug.Log ("Center is " + bounds.center);
			vg = new VoxelGrid(center, bounds.size,size);
			voxels = vg.GenerateVoxels (mesh);
			float now = Time.realtimeSinceStartup;
			float diff = now - then;
			Debug.Log ("CPU Voxelization took " + diff + " seconds");
			Debug.Log ("Generated " + voxels.Count + " voxels");
			if (drawVoxels) {
				foreach(GameObject go in voxelsInstance) {
					Destroy (go);
				}
				voxelsInstance.Clear ();
				Vector3 vecsize = new Vector3 (size, size, size);
				foreach (Voxel voxel in voxels) {
					GameObject b = Instantiate (voxelInstance, voxel.position, new Quaternion ());
					b.transform.localScale = vecsize;
					b.transform.parent = transform;
					voxelsInstance.Add (b);
				}
			}
			refresh = false;
		}

		if (drawGrid) {
			vg.DrawBorders ();
		}



	}
}
