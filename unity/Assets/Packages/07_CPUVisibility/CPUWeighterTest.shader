﻿Shader "Unlit/CPUWeighterTest"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Cull Back
			ZWrite On
			ZTest LEqual
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			float4x4 _MATRIX_M;
			float4x4 _MATRIX_V;
			float4x4 _MATRIX_P;

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(_MATRIX_P, mul(_MATRIX_V, mul(_MATRIX_M, v.vertex)));
				return o;
			}

			fixed4 frag (v2f i, uint pid : SV_PrimitiveID) : SV_Target
			{
				float id = (1.0f / 1808.0f) * (float)(pid - 1);
				return fixed4(id, 0.0f, 0.0f, 1.0f);
			}
			ENDCG
		}
	}
}
