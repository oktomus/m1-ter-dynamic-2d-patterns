﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine.Rendering;

/*
NE FONCTIONNE PAS

Les formats de textures sont limites et ne permettent pas de recuperer dans uint sans overflow. 
Les valeurs des triangles sont donc tronqués. 
 */

public class CPUVisibilityTest : MonoBehaviour, IPreFrameJob {

	public Material weighterMaterial;
    public Material renderTextureDebugger;
	private ComputeBuffer samplesBuffer;

    public bool render;
    public bool continuous;

	private Mesh mesh;

    public int depth = 8;

    public float samplesize = 0.1f;

    public bool showPoints = false;

    private List<Vector3> trianglesPos;


    private RenderTexture rtex;

    private GPUSampling.Kernel computeLink;

    private int width, height;

    private List<bool> trianglesVisibility;
    private RenderTextureFormat format;
    //private Texture2D rendered_triangles;

	void Start () {
        Display mainDisp = Display.main;
        width = mainDisp.renderingWidth;
        height = mainDisp.renderingHeight;
        format = RenderTextureFormat.RFloat;
        TextureFormat tex_format = TextureFormat.RFloat;

        if (!SystemInfo.SupportsRenderTextureFormat(
            format
        ))
        {
            Debug.LogError("Render format is not supported");
            return;
        }

        if (!SystemInfo.SupportsTextureFormat(
            tex_format
        ))
        {
            Debug.LogError("Texture format is not supported");
            return;
        }
        // Attach pre frame job
        Camera.main.GetComponent<CameraPreJobs>().AddPreJob(this);

        mesh = GetComponent<MeshFilter>().mesh;
        trianglesVisibility = new List<bool>();
        trianglesPos = new List<Vector3>();
        //rendered_triangles = new Texture2D(width, height, tex_format, false);

        // Fill triangle positions
        for(int i = 0; i < mesh.triangles.Length; i+=3)
        {
            int ia = mesh.triangles[i];
            int ib = mesh.triangles[i + 1];
            int ic = mesh.triangles[i + 2];

            Vector3 a = mesh.vertices[ia];
            Vector3 b = mesh.vertices[ib];
            Vector3 c = mesh.vertices[ic];

            trianglesPos.Add((a + b + c) / 3.0f);
            trianglesVisibility.Add(false);
        }

        // Create compute buffer
        Debug.Log((trianglesVisibility.Count) + " triangles");
    }

    public void PreFrameJob()
    {
        if(!render || mesh == null) return;
        if(!continuous) render = false;
        Debug.Log("Starting pre frame job");
        //width = height = 10;

        // Render
        float then = Time.realtimeSinceStartup;
        if(rtex == null)
        {
            // Create render texture
            rtex = new RenderTexture(
                width, height, depth, format, RenderTextureReadWrite.Linear
            );
        }
        // Clear previous render
        RenderTexture rt = UnityEngine.RenderTexture.active;
        UnityEngine.RenderTexture.active = rtex;
        GL.Clear(true, true, Color.clear);
        UnityEngine.RenderTexture.active = rt;
        // Debug texture on plain
        if(renderTextureDebugger != null && renderTextureDebugger.mainTexture != rtex)
        {
            renderTextureDebugger.mainTexture = rtex;
        }
        // Real job 
        Graphics.SetRenderTarget(rtex);
        if(!weighterMaterial.SetPass(0))
        {
            Debug.LogError("Failed to set rendering path");
            return;
        }
        // Set correct matrix
        Matrix4x4 P = GL.GetGPUProjectionMatrix(Camera.main.projectionMatrix, true);
        Matrix4x4 V = Camera.main.worldToCameraMatrix;
        Matrix4x4 M = transform.localToWorldMatrix;
        weighterMaterial.SetMatrix("_MATRIX_M", M);
        weighterMaterial.SetMatrix("_MATRIX_V", V);
        weighterMaterial.SetMatrix("_MATRIX_P", P);
        weighterMaterial.SetFloat("_triangleCount", trianglesVisibility.Count);
        // DRAW !
        Graphics.DrawMeshNow(mesh, transform.position, transform.rotation);

        float diff = Time.realtimeSinceStartup - then;
        Debug.Log("Rendering took " + diff + " seconds");

        // Read rendered texture
        /* 
        RenderTexture rt_active = RenderTexture.active;
        RenderTexture.active = rtex;
        rendered_triangles.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        rendered_triangles.Apply();
        RenderTexture.active = rt_active;
        renderTextureDebugger.mainTexture = rendered_triangles;

        // Reset weights
        for(int i = 0, n = trianglesVisibility.Count; i < n; ++i)
        {
            trianglesVisibility[i] = false;
        }

        // Read data
        for(int x = 0, nx = width; x < nx; ++x)
        {
            for (int y = 0, ny = height; y < ny; ++y)
            {
                Color pc = rendered_triangles.GetPixel(x, y);
                if(pc.r == 0.0f) continue;
                int id = (int)(pc.r * (float)(trianglesVisibility.Count));
                if (id > trianglesVisibility.Count - 1)
                {
                    Debug.LogWarning(id  + " out of bounds. ");
                    Debug.LogWarning("Color: " + pc);
                }
                trianglesVisibility[id] = true;
            }
        }
        */

        Debug.Log("End pre frame job");
        // Step
    }

    void OnDrawGizmos()
    {
        if(mesh == null || !showPoints) return;
        for(int i = 0; i < trianglesPos.Count; ++i)
        {
            bool flag = trianglesVisibility[i];
            Vector3 pos = trianglesPos[i];
            if(flag) Gizmos.color = Color.green;
            else Gizmos.color = Color.red;
            Gizmos.DrawSphere(pos, samplesize);
        }
    }

}
