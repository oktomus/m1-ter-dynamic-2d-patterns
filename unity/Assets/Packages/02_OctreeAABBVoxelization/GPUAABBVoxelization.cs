﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Rendering;

using GPUSampling;

public class GPUAABBVoxelization : MonoBehaviour {

	[SerializeField] protected Mesh source;
	[SerializeField] protected ComputeShader shader;
	[SerializeField] protected int countByAxis = 32;
	[SerializeField] protected bool showVoxels = false;

	private Vector3 voxelSize = new Vector3();
	private List<Vector3> voxels = new List<Vector3>();

	void Start () {
			string kComputeKey = "Voxelize";
			string kStartKey = "_Start", kEndKey = "_End", kSizeKey = "_Size";
			string kUnitKey = "_Unit", kHalfUnitKey = "_HalfUnit";
			string kWidthKey = "_Width", kHeightKey = "_Height", kDepthKey = "_Depth";
			string kTriCountKey = "_TrianglesCount";
			string kVertBufferKey = "_VertBuffer", kTriBufferKey = "_TriBuffer";
			string kOutputBufferKey = "_VoxelBuffer";
			// Pre process
			source.RecalculateBounds ();

			// Size utils
			Bounds bounds = source.bounds;
			float BBSize = Mathf.Max (bounds.size.x, 
				Mathf.Max (bounds.size.y, bounds.size.z));
			float VSize = BBSize / countByAxis;
			voxelSize = new Vector3(VSize, VSize, VSize);

			// Buffer setup
			ComputeBuffer verticesBuffer = new ComputeBuffer (source.vertices.Length,
				Marshal.SizeOf (typeof(Vector3)));
			verticesBuffer.SetData (source.vertices);

			ComputeBuffer trisBuffer = new ComputeBuffer (source.triangles.Length,
				Marshal.SizeOf (typeof(int)));
			trisBuffer.SetData (source.triangles);

			ComputeBuffer outputBuffer = new ComputeBuffer (
				// Number of voxel on each axis
				Mathf.CeilToInt ((bounds.size.x / BBSize * countByAxis)) *
				Mathf.CeilToInt ((bounds.size.y / BBSize * countByAxis)) *
				Mathf.CeilToInt ((bounds.size.z / BBSize * countByAxis)),
				Marshal.SizeOf (typeof(Voxel))
			);

			// Compute function link
			Kernel kernel = new Kernel(shader, kComputeKey);

			// Map bounds on GPU
			shader.SetVector(kStartKey, bounds.min);
			shader.SetVector(kEndKey, bounds.max);
			shader.SetVector(kSizeKey, bounds.size);

			Vector3 realSize = bounds.size;
			int width = Mathf.CeilToInt (realSize.x / VSize);
			int height = Mathf.CeilToInt (realSize.y / VSize);
			int depth= Mathf.CeilToInt (realSize.z / VSize);

			shader.SetFloat (kUnitKey, VSize);
			shader.SetFloat (kHalfUnitKey, VSize * 0.5f);
			shader.SetInt (kWidthKey, width);
			shader.SetInt (kHeightKey, height);
			shader.SetInt (kDepthKey, depth);

			// Map mesh on GPU
			shader.SetBuffer(kernel.Index, kVertBufferKey, verticesBuffer);
			shader.SetInt (kTriCountKey, trisBuffer.count);
			shader.SetBuffer (kernel.Index, kTriBufferKey, trisBuffer);
			shader.SetBuffer (kernel.Index, kOutputBufferKey, outputBuffer);

			// GO
			shader.Dispatch (kernel.Index, 
				width / (int)kernel.ThreadX + 1,
				height / (int)kernel.ThreadY + 1,
				depth / (int)kernel.ThreadZ + 1);

			// Free
			verticesBuffer.Dispose();
			trisBuffer.Dispose ();

			var gpubuffer = new GPUVoxels (outputBuffer, VSize);
			var data = gpubuffer.GetVoxels();

			for(int i = 0; i < data.Length; i++) {
				if(data[i].flag) {
					voxels.Add(data[i].position);
				}
			}
			gpubuffer.Dispose();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnDrawGizmos () {
		if(showVoxels) {
			Gizmos.color = Color.red;
			for(int v = 0; v < voxels.Count; v++) {
				Gizmos.DrawCube(voxels[v], voxelSize);
			}
		}
	}
}
