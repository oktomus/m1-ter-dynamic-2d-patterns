﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class OctreeAABBVoxelization : MonoBehaviour {

	[SerializeField] protected Mesh mesh;
	[SerializeField] protected int count = 32;
	[SerializeField] protected bool showVoxels = false;

	public class Point{ }
	public class Triangle{		
		public int id;
		public Vector3 a, b, c;
		public Triangle(int pid, Vector3 a, Vector3 b, Vector3 c) {
			id = pid;
			this.a = a;
			this.b = b;
			this.c = c;
		}
	}

	PointOctree<Triangle> pointTree = null;
	List<Vector3> voxels = new List<Vector3> ();
	Vector3 voxelSize = new Vector3();



	// Use this for initialization
	void Start () {
		// Build octree
		float then = Time.realtimeSinceStartup;
		mesh.RecalculateBounds ();
		Bounds bounds = mesh.bounds;
		float BBSize = Mathf.Max (bounds.size.x, 
			Mathf.Max (bounds.size.y, bounds.size.z));
		float VSize = BBSize / count;
		float HVSize = VSize * 0.5f;
		voxelSize = new Vector3(VSize, VSize, VSize);

		float maxdepth = 1.2f;
		Debug.Log ("Minimum node size: " + maxdepth);
		pointTree = new PointOctree<Triangle> (BBSize, bounds.center, maxdepth);

		// Actual process
		for (int i = 0; i < mesh.triangles.Length; i += 3) {
			int ida = mesh.triangles[i];
			int idb = mesh.triangles[i + 1];
			int idc = mesh.triangles[i + 2];
			Vector3 a = mesh.vertices[ida];
			Vector3 b = mesh.vertices[idb];
			Vector3 c = mesh.vertices[idc];
			Vector3 center = (a + b + c) / 3.0f;
			pointTree.Add (new Triangle(i, a, b, c), center);
		}


		float diff = Time.realtimeSinceStartup - then;
		float suboctree, subtriAABB;
		float totalOctreeProcess = 0.0f, totalAABBTriangleProcess = 0.0f;
		Debug.Log ("Building the tree took " + diff + " seconds");

		// Voxelization
		then = Time.realtimeSinceStartup;

		// Lets do the first slice only first
		int xcount = Mathf.CeilToInt ((bounds.size.x / BBSize * count));
		int ycount = Mathf.CeilToInt ((bounds.size.y / BBSize * count));
		int zcount = Mathf.CeilToInt ((bounds.size.z / BBSize * count));

		Vector3 voxelPos = new Vector3();
		Vector3 voxelHalfSize = new Vector3(
			HVSize, HVSize, HVSize
		);
		for(int x = 0; x < xcount; x++) {
			for(int y = 0; y < ycount; y++) {
				for(int z = 0; z < zcount; z++) {
					// Calculte voxel position
					voxelPos.x = bounds.min.x + VSize * x + HVSize;
					voxelPos.y = bounds.min.y + VSize * y + HVSize;
					voxelPos.z = bounds.min.z + VSize * z + HVSize;
					// Find closests triangles
					suboctree = Time.realtimeSinceStartup;
					List<Triangle> closest =  pointTree.GetNearby(voxelPos, VSize * 4);
					totalOctreeProcess += Time.realtimeSinceStartup - suboctree;

					// Nothing ?
					if(closest.Count == 0) continue;

					// Calculate intersection
					for(int t = 0; t < closest.Count; t++) {
						Triangle triangle = closest[t];
						subtriAABB = Time.realtimeSinceStartup;
						if(triBoxOverlap(voxelPos, voxelHalfSize, triangle.a , triangle.b, triangle.c)){
							totalAABBTriangleProcess += Time.realtimeSinceStartup - subtriAABB;
							voxels.Add(voxelPos);
							// YES !
							break;
						}
						totalAABBTriangleProcess += Time.realtimeSinceStartup - subtriAABB;

					}
				}
			}
		}

		diff = Time.realtimeSinceStartup - then;
		Debug.Log ("Voxelization took " + diff + " seconds");
		Debug.Log("Octree total root time: " + totalOctreeProcess + " seconds");
		Debug.Log("Total intersection test time: " + totalAABBTriangleProcess + " seconds");
		
	}
	
	
	// Update is called once per frame
	void OnDrawGizmos () {
		if(pointTree != null)
			pointTree.DrawAllBounds ();
		if(showVoxels) {
			Gizmos.color = Color.red;
			for(int v = 0; v < voxels.Count; v++) {
				Gizmos.DrawCube(voxels[v], voxelSize);
			}
		}
	}

	// 

	bool planeBoxOverlap(Vector3 normal, Vector3 point, Vector3 box) {
		Vector3 vmin, vmax;

		// X
		if (normal.x > 0.0f) {
			vmin.x = -box.x - point.x;
			vmax.x = box.x - point.x;
		} else { 
			vmin.x = box.x - point.x;
			vmax.x = -box.x - point.x;
		}
		// Y
		if (normal.y > 0.0f) {
			vmin.y = -box.y - point.y;
			vmax.y = box.y - point.y;
		} else { 
			vmin.y = box.y - point.y;
			vmax.y = -box.y - point.y;
		}
		// Z
		if (normal.z > 0.0f) {
			vmin.z = -box.z - point.z;
			vmax.z = box.z - point.z;
		} else { 
			vmin.z = box.z - point.z;
			vmax.z = -box.z - point.z;
		}

		if (Vector3.Dot (normal, vmin) > 0.0f)
			return false;
		if (Vector3.Dot (normal, vmax) >= 0.0f)
			return true;

		return false;
	}


	bool triBoxOverlap(Vector3 boxcenter, Vector3 boxhsize, Vector3 va, Vector3 vb, Vector3 vc) {

		float p0, p1, p2, rad, min, max;

		// Translate triangle so that boxcenter is at 0,0,0
		va -= boxcenter;
		vb -= boxcenter;
		vc -= boxcenter;

		// Compute edges
		Vector3 e0 = vb - va;
		Vector3 e1 = vc - vb;
		Vector3 e2 = va - vc;


		// Crossproduct test
		float fex = Mathf.Abs(e0.x);
		float fey = Mathf.Abs(e0.y);
		float fez = Mathf.Abs(e0.z);

		// Utils functions
		Func<float, float, float, float, bool> AXISTEST_X01 = (a, b, fa, fb) =>{
			p0 = a*va.y - b*va.z;			       	   
			p2 = a*vc.y - b*vc.z;			       	   
        	if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} 
			rad = fa * boxhsize.y + fb * boxhsize.z;   
			if(min>rad || max<-rad) return false;
			return true;
		};
		Func<float, float, float, float, bool> AXISTEST_X2 = (a, b, fa, fb) =>{
			p0 = a*va.y - b*va.z;			           
			p1 = a*vb.y - b*vb.z;			       	   
        	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} 
			rad = fa * boxhsize.y + fb * boxhsize.z;   
			if(min>rad || max<-rad) return false;
			return true;
		};
		Func<float, float, float, float, bool> AXISTEST_Y02 = (a, b, fa, fb) =>{
			p0 = -a*va.x + b*va.z;		      	   
			p2 = -a*vc.x + b*vc.z;	       	       	   
        	if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} 
			rad = fa * boxhsize.x + fb * boxhsize.z;   
			if(min>rad || max<-rad) return false;
			return true;
		};
		Func<float, float, float, float, bool> AXISTEST_Y1 = (a, b, fa, fb) =>{
			p0 = -a*va.x + b*va.z;		      	   
			p1 = -a*vb.x + b*vb.z;	     	       	   
        	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} 
			rad = fa * boxhsize.x + fb * boxhsize.z;   
			if(min>rad || max<-rad) return false;
			return true;
		};
		Func<float, float, float, float, bool> AXISTEST_Z12 = (a, b, fa, fb) =>{
			p1 = a*vb.x - b*vb.y;			           
			p2 = a*vc.x - b*vc.y;			       	   
        	if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} 
			rad = fa * boxhsize.x + fb * boxhsize.y;   
			if(min>rad || max<-rad) return false;
			return true;
		};
		Func<float, float, float, float, bool> AXISTEST_Z0 = (a, b, fa, fb) =>{
			p0 = a*va.x - b*va.y;				   
			p1 = a*vb.x - b*vb.y;			           
        	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} 
			rad = fa * boxhsize.x + fb * boxhsize.y;   
			if(min>rad || max<-rad) return false;
			return true;
		};

		// Go Real test with edges

		//    AXISTEST_X01(e0[Z], e0[Y], fez, fey);
		if(!AXISTEST_X01(e0.z, e0.y, fez, fey)) return false;
		//    AXISTEST_Y02(e0[Z], e0[X], fez, fex);
		if(!AXISTEST_Y02(e0.z, e0.x, fez, fex)) return false;
		//    AXISTEST_Z12(e0[Y], e0[X], fey, fex);
		if(!AXISTEST_Z12(e0.y, e0.x, fey, fex)) return false;

		fex = Mathf.Abs(e1.x);
		fey = Mathf.Abs(e1.y);
		fez = Mathf.Abs(e1.z);
		// AXISTEST_X01(e1[Z], e1[Y], fez, fey);
		if(!AXISTEST_X01(e1.z, e1.y, fez, fey)) return false;
   		// AXISTEST_Y02(e1[Z], e1[X], fez, fex);
		if(!AXISTEST_Y02(e1.z, e1.x, fez, fex)) return false;
   		// AXISTEST_Z0(e1[Y], e1[X], fey, fex);
		if(!AXISTEST_Z0(e1.y, e1.x, fey, fex)) return false;

		fex = Mathf.Abs(e2.x);
		fey = Mathf.Abs(e2.y);
		fez = Mathf.Abs(e2.z);
		//    AXISTEST_X2(e2[Z], e2[Y], fez, fey);
		if(!AXISTEST_X2(e2.z, e2.y, fez, fey)) return false;
   		// AXISTEST_Y1(e2[Z], e2[X], fez, fex);
		if(!AXISTEST_Y1(e2.z, e2.x, fez, fex)) return false;
   		// AXISTEST_Z12(e2[Y], e2[X], fey, fex);
		if(!AXISTEST_Z12(e2.y, e2.x, fey, fex)) return false;

		// End test 01
		/*  first test overlap in the {x,y,z}-directions */
		/*  find min, max of the triangle each direction, and test for overlap in */
		/*  that direction -- this is equivalent to testing a minimal AABB around */
		/*  the triangle against the AABB */

		// X TEST
		min = Mathf.Min(va.x, vb.x, vc.x);
		max = Mathf.Max(va.x, vb.x, vc.x);
		if(min > boxhsize.x ||max < -boxhsize.x) return false;

		// Y TEST
		min = Mathf.Min(va.y, vb.y, vc.y);
		max = Mathf.Max(va.y, vb.y, vc.y);
		if(min > boxhsize.y ||max < -boxhsize.y) return false;

		// Z TEST
		min = Mathf.Min(va.z, vb.z, vc.z);
		max = Mathf.Max(va.z, vb.z, vc.z);
		if(min > boxhsize.z ||max < -boxhsize.z) return false;

		/*  test if the box intersects the plane of the triangle */
		/*  compute plane equation of triangle: normal*x+d=0 */
		Vector3 normal = Vector3.Cross(e0, e1);
		if(!planeBoxOverlap(normal, va, boxhsize)) return false;
		

		return true;
	}

}